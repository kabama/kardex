package com.kbm.kardex.infrastucture.repository.inventario;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kbm.kardex.infrastucture.entities.inventario.Categoria;

/**
 * Clase con las pruebas unitarias de {@link CategoriaRepository}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@DataJpaTest
public class CategoriaRepositoryTest
{
    @Autowired
    private CategoriaRepository categoriaRepository;
    
    /**
     * Prueba que:
     * cuando: Consultar Categorias
     * then: retorna Un Listado Categorias
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Test
    public void cuando_ConsultarCategorias_then_retornaUnListadoCategorias() {
        List<Categoria> unidades = (List<Categoria>) categoriaRepository.findAll();
        assertThat(unidades).isNotEmpty();
    }
    
    /**
     * Prueba que:
     * cuando: ConsultarCategorias
     * then: retorna Un Listado De 4 Categorias Iniciales
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Test
    public void cuando_ConsultarCategorias_then_retornaUnListadoCategoriasDe4CategoriasIniciales() {
        List<Categoria> unidades = (List<Categoria>) categoriaRepository.findAll();
        final int unidadesIniciales = 4;
        assertThat(unidades).hasSize(unidadesIniciales);
    }
}
