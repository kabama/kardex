package com.kbm.kardex.infrastucture.repository.inventario;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kbm.kardex.infrastucture.entities.inventario.Categoria;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;
import com.kbm.kardex.infrastucture.entities.inventario.Unidad;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@DataJpaTest
class ProductoRepositoryTest
{
    /** Repositorio para el manejo de productos */
    @Autowired
    private ProductoRepository  productoRepository;
    /** Repositorio para el manejo de categorias */
    @Autowired
    private CategoriaRepository categoriaRepository;
    /** Repositorio para el manejo de unidades */
    @Autowired
    private UnidadRepository unidadRepository;
    
    @Test
    void when_ConsultarProductos_then_ListadoDeProductos()
    {
        List<Producto> productos = (List<Producto>) productoRepository.findAll();
        assertThat(productos).isNotEmpty();
    }
    
    @Test
    void when_ConsultarProductoIniciales_then_ListadoDeProductos()
    {
        List<Producto> productos = (List<Producto>) productoRepository.findAll();
        assertThat(productos).hasSize(20);
    }
    
    @Test
    void when_InsertarUnProducto_then_ObtenemosUnNuevoProductoNoNulo()
    {
        Optional<Categoria> categoria = categoriaRepository.findById(1L);
        Unidad unidad = unidadRepository.findById(1L).get();
        Producto producto = Producto.builder()
                .categoria(categoria.get()).cantidad(20)
                .nombre("Acer 220")
                .descripcion("Nuevo producto")
                .codigo("201")
                .costo(500.0)
                .marca("XX-A")
                .modelo("01-XXX-a")
                .observacion("Producto de la mejor calidad").unidad(unidad).build();
        producto = productoRepository.save(producto);
        assertThat(producto).isNotNull();
    }
}
