package com.kbm.kardex.infrastucture.repository.movimientos;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.entities.movimientos.Movimiento;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;

/**
 * Clase con pruebas unitarias para {@link MovimientoRepository}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@DataJpaTest
public class MovimientoRepositoryTest
{
    @Autowired
    private MovimientoRepository movimientoRepository;
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Test
    void when_insertarMovimiento_then_SeCreaMovimientoNuevo()
    {
        Usuario usuario = usuarioRepository.findById(1L).get();
        Movimiento movimiento = Movimiento.builder().usuario(usuario).observaciones("Compra de productos").build();
        movimientoRepository.save(movimiento);
        assertThat(movimiento.getId()).isNotNull();
    }
}
