package com.kbm.kardex.infrastucture.repository.movimientos;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kbm.kardex.infrastucture.entities.movimientos.DetalleMovimiento;
import com.kbm.kardex.infrastucture.repository.movimientos.DetalleMovimientoRepository;

/**
 * Clase para pruebas unitarias de {@link DetalleMovimientoRepository}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@DataJpaTest
class DetalleMovimientoRepositoryTest
{
 @Autowired
 private DetalleMovimientoRepository detalleMovimientoRepository;
 
 @Test
 void when_buscarUltimoDetalleMovimientoPorIdMovimiento_then_DevuelveDetalleMovimiento() {
     DetalleMovimiento detalle = detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(1L);
     assertThat(detalle.getCantidad()).isEqualTo(50);
 }
}
