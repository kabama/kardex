package com.kbm.kardex.infrastucture.repository.inventario;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kbm.kardex.infrastucture.entities.inventario.Unidad;

/**
 * Clase con las pruebas unitarias de {@link UnidadRepository}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@DataJpaTest
public class UnidadRepositoryTest
{
    @Autowired
    private UnidadRepository unidadRepository;
    
    /**
     * Prueba que:
     * cuando: ConsultarUnidades
     * then: retorna Un Listado Unidades
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Test
    public void cuando_ConsultarUnidades_then_retornaUnListadoUnidades() {
        List<Unidad> unidades = (List<Unidad>) unidadRepository.findAll();
        assertThat(unidades).isNotEmpty();
    }
    
    /**
     * Prueba que:
     * cuando: ConsultarUnidades
     * then: retorna Un Listado De 4 Unidades Iniciales
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Test
    public void cuando_ConsultarUnidades_then_retornaUnListadoUnidadesDe4UnidadesIniciales() {
        List<Unidad> unidades = (List<Unidad>) unidadRepository.findAll();
        final int unidadesIniciales = 4;
        assertThat(unidades).hasSize(unidadesIniciales);
    }
}
