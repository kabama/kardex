package com.kbm.kardex.infrastucture.repository.administrativo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;

/**
 * Clase con las pruebas unitarias de {@link UsuarioRepository}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@DataJpaTest
class UsuarioRepositoryTest
{
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    
    @Test
    void when_consultarTodosLosUsuario_then_retornaListadoVacio() {
        List<Usuario> usuario = usuarioRepository.findAll();
        assertThat(usuario).isNotEmpty();
    }
}
