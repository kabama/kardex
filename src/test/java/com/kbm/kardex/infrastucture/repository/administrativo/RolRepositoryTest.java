package com.kbm.kardex.infrastucture.repository.administrativo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.kbm.kardex.infrastucture.entities.administrativo.Rol;

@DataJpaTest
public class RolRepositoryTest
{
    @Autowired
    private RolRepository rolRepository;
    
    
    @Test
    void when_consultarTodosLosUsuario_then_retornaListadoVacio() {
        List<Rol> roles = (List<Rol>) rolRepository.findAll();
        assertThat(roles).isNotEmpty();
    }
}
