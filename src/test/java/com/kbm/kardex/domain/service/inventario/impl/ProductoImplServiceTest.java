package com.kbm.kardex.domain.service.inventario.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.kbm.kardex.TestConfig;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.inventario.dto.ProductoDto;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;
import com.kbm.kardex.infrastucture.repository.inventario.ProductoRepository;

/**
 * Clase con pruebas unitarias de {@link ProductoImplService}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@TestConfig
class ProductoImplServiceTest
{
    private static final String FIND_BY_MARCA_TEXT = "AA";
    private static final String FIND_BY_NAME_TEXT = "A1";
    private static final String FIND_BY_CODIGO_TEXT = "001";

    @Mock
    private ProductoRepository productoRepository;
    
    @InjectMocks
    private ProductoImplService productoImplService;
    
    @Test
    void when_findByMarca_then_retornaListadoProductosPorMarca() {
        List<Producto> productos = new ArrayList<>();
        Producto producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A1");
        producto.setMarca(FIND_BY_MARCA_TEXT);
        productos.add(producto);
        
        producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A2");
        producto.setMarca(FIND_BY_MARCA_TEXT);
        productos.add(producto);
 
        when(this.productoRepository.findByMarca(FIND_BY_MARCA_TEXT)).thenReturn(productos);
        
        List<ProductoDto> buscarMarcaResultado = this.productoImplService.findByMarca(FIND_BY_MARCA_TEXT);
        assertThat(buscarMarcaResultado).isNotEmpty();
        assertThat(buscarMarcaResultado.get(0).getMarca()).isEqualTo(FIND_BY_MARCA_TEXT);
    }
    
    
    @Test
    void when_findByMarca_then_NoFoundException() {
        when(this.productoRepository.findByMarca(FIND_BY_MARCA_TEXT)).thenReturn(Collections.emptyList());
        
        Assertions.assertThrows(DataNotFoundException.class, () ->  this.productoImplService.findByMarca(FIND_BY_MARCA_TEXT));
    }
    
    
    
    @Test
    void when_findByNombre_then_retornaListadoProductosPorNombre() {
        List<Producto> productos = new ArrayList<>();
        Producto producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A1");
        producto.setMarca("XS");
        productos.add(producto);
        
        producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A1");
        producto.setMarca("XX");
        productos.add(producto);
 
        when(this.productoRepository.findByNombre(FIND_BY_NAME_TEXT)).thenReturn(productos);
        
        List<ProductoDto> buscarMarcaResultado = this.productoImplService.findByNombre(FIND_BY_NAME_TEXT);
        assertThat(buscarMarcaResultado).isNotEmpty();
        assertThat(buscarMarcaResultado.get(0).getNombre()).isEqualTo(FIND_BY_NAME_TEXT);
    }
    
    
    @Test
    void when_findByNombre_then_NoFoundException() {
        when(this.productoRepository.findByMarca(FIND_BY_NAME_TEXT)).thenReturn(Collections.emptyList());
        
        Assertions.assertThrows(DataNotFoundException.class, () ->  this.productoImplService.findByNombre(FIND_BY_NAME_TEXT));
    }
    
    
    @Test
    void when_findByCodigo_then_retornaListadoProductosPorMarca() {
        List<Producto> productos = new ArrayList<>();
        Producto producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A1");
        producto.setMarca("XS");
        producto.setCodigo("001");
        productos.add(producto);
        
        producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A1");
        producto.setMarca("XX");
        producto.setCodigo("001");
        productos.add(producto);
 
        when(this.productoRepository.findByCodigo(FIND_BY_CODIGO_TEXT)).thenReturn(productos);
        
        List<ProductoDto> buscarMarcaResultado = this.productoImplService.findByCodigo(FIND_BY_CODIGO_TEXT);
        assertThat(buscarMarcaResultado).isNotEmpty();
        assertThat(buscarMarcaResultado.get(0).getCodigo()).isEqualTo(FIND_BY_CODIGO_TEXT);
    }
    
    
    @Test
    void when_findByCodigo_then_NoFoundException() {
        when(this.productoRepository.findByCodigo(FIND_BY_CODIGO_TEXT)).thenReturn(Collections.emptyList());
        
        Assertions.assertThrows(DataNotFoundException.class, () ->  this.productoImplService.findByCodigo(FIND_BY_CODIGO_TEXT));
    }
    
    
    @Test
    void when_findAll_then_retornaListadoProductos() {
        List<Producto> productos = new ArrayList<>();
        Producto producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A1");
        producto.setMarca("XS");
        producto.setCodigo("001");
        productos.add(producto);
        
        producto = new Producto();
        producto.setId(1L);
        producto.setNombre("A1");
        producto.setMarca("XX");
        producto.setCodigo("001");
        productos.add(producto);
 
        when(this.productoRepository.findAll()).thenReturn(productos);
        
        List<ProductoDto> buscarMarcaResultado = this.productoImplService.findAll();
        assertThat(buscarMarcaResultado).isNotEmpty();
    }
    
    
    @Test
    void when_findAll_then_NoFoundException() {
        when(this.productoRepository.findAll()).thenReturn(Collections.emptyList());
        
        Assertions.assertThrows(DataNotFoundException.class, () ->  this.productoImplService.findAll());
    }
}
