package com.kbm.kardex.domain.service.inventario.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.kbm.kardex.TestConfig;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.inventario.dto.CategoriaDto;
import com.kbm.kardex.infrastucture.entities.inventario.Categoria;
import com.kbm.kardex.infrastucture.repository.inventario.CategoriaRepository;

/**
 * Clase con puebas unitarias de {@link CategoriaImplService}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/28
 * @since 0.0.1 2020/10/28
 */
@TestConfig
class CategoriaImplServiceTest
{
    
    @InjectMocks
    private CategoriaImplService categoriaService;
    
    @Mock
    private CategoriaRepository categoriaRepository;
    
    @Test
    void when_ConsultarCategorias_then_retornaListadoCategoriasDto()
    {
        List<Categoria> categorias = new ArrayList<>();
        Categoria categoria = new Categoria();
        categoria.setId(1L);
        categoria.setNombre("Zapatos");
        categoria.setDescripcion("Zapatos");
        categorias.add(categoria);
        when(this.categoriaRepository.findAll()).thenReturn(categorias);
        List<CategoriaDto> resultadoCategorias = this.categoriaService.consultarCategorias();
        assertThat(resultadoCategorias).isNotEmpty();
        
    }
    
    
    @Test
    void when_ConsultarCategorias_then_retornaListadoVacio()
    {
        when(this.categoriaRepository.findAll()).thenReturn(Collections.emptyList());
        Assertions.assertThrows(DataNotFoundException.class, () -> this.categoriaService.consultarCategorias());
        
    }
}
