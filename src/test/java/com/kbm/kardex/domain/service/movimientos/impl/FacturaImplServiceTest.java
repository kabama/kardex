package com.kbm.kardex.domain.service.movimientos.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.kbm.kardex.TestConfig;
import com.kbm.kardex.commons.enums.StatusModelo;
import com.kbm.kardex.domain.exceptions.BadRequestException;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearDetalleFacturaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaRespuestaDto;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.entities.inventario.Categoria;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;
import com.kbm.kardex.infrastucture.entities.inventario.Unidad;
import com.kbm.kardex.infrastucture.entities.movimientos.DetalleFactura;
import com.kbm.kardex.infrastucture.entities.movimientos.Factura;
import com.kbm.kardex.infrastucture.entities.movimientos.TipoMovimiento;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;
import com.kbm.kardex.infrastucture.repository.inventario.ProductoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.DetalleFacturaRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.FacturaRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.TipoMovimientoRepository;

/**
 * Clase con pruebas unitarias de {@link FacturaImplSerBahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@TestConfig
class FacturaImplServiceTest
{
    
    @Mock
    private FacturaRepository        facturaRepository;
    @Mock
    private DetalleFacturaRepository detalleFacturaRepository;
    @Mock
    private UsuarioRepository        usuarioRepository;
    @Mock
    private TipoMovimientoRepository tipoMovimientoRepository;
    @Mock
    private ProductoRepository productoRepository;
    
    @InjectMocks
    private FacturaImplService facturaImplService;
    
    private Producto producto = new Producto();
    
    @BeforeEach
    void init() {
        producto = Producto.builder().id(2L).unidad(Unidad.builder().id(1L).nombre("Unidad").build()).build();
        when(productoRepository.findById(producto.getId())).thenReturn(Optional.of(producto));
    }
    
    @Test
    void when_findByProductoId_then_retornaListadoDetalles()
    {
        
        ConsultarDetalleFacturaPeticionDto peticionConsultar = mapearPeticionConsultarDetalleFactura();
        List<DetalleFactura> detalles = mapearDetalleFactura();
        when(detalleFacturaRepository.findByProductoIdAndStatus(peticionConsultar.getIdProducto(), StatusModelo.ACTIVO.getStatus())).thenReturn(detalles);
        List<ConsultarDetalleFacturaRespuestaDto> respuestaConsultar = this.facturaImplService.findByProductoId(peticionConsultar);
        assertThat(respuestaConsultar).isNotEmpty();
    }
    
    @Test
    void when_findByProductoId_then_NotFoundException()
    {
        ConsultarDetalleFacturaPeticionDto peticionConsultar = mapearPeticionConsultarDetalleFactura();
        when(detalleFacturaRepository.findByProductoIdAndStatus(peticionConsultar.getIdProducto(), StatusModelo.ACTIVO.getStatus())).thenReturn(null);
        Assertions.assertThrows(DataNotFoundException.class, () -> this.facturaImplService.findByProductoId(peticionConsultar));
    }
    
    @Test
    void when_crearFactura_then_returnCrearFacturaRespuestaDto()
    {
        TipoMovimiento tipoMovimiento = TipoMovimiento.builder().id(1L).nombre("Compra").build();
        CrearFacturaPeticionDto peticionCrear = mapearPeticionCrearfactura();
        Usuario usuario = Usuario.builder().id(1L).nombreCompleto("Usuario 1").build();
        when(usuarioRepository.findById(peticionCrear.getIdProveedor())).thenReturn(Optional.of(usuario));
 
        
        Factura factura = Factura.builder().fechaCreacion(peticionCrear.getFechaCreacion()).numeroFactura(peticionCrear.getNumeroFactura()).proveedor(usuario)
                .tipoMovimiento(tipoMovimiento).build();
 
        
        
        when(tipoMovimientoRepository.findById(peticionCrear.getIdTipoMovimiento())).thenReturn(Optional.of(tipoMovimiento));

        Set<DetalleFactura> detalles = mapearDetalleFacturaCuandoCrearFactura(factura);
        factura.setDetalleFactura(detalles);
        when(facturaRepository.save(factura)).thenReturn(factura);
        
        
        CrearFacturaRespuestaDto respuestaCrearFactura = this.facturaImplService.crearFactura(peticionCrear);
        assertThat(respuestaCrearFactura).isNotNull();
    }
    
    
    @Test
    void when_listaFacturas_then_retornaListadoCrearFacturaRespuestaDto() {
        when(this.facturaRepository.findAll()).thenReturn(mapearListadofactura());
        List<CrearFacturaRespuestaDto> respuestaListado = this.facturaImplService.listaFacturas();
        assertThat(respuestaListado).isNotEmpty();
    }
    
    @Test
    void when_listaFacturas_then_retornaNotFoundException() {
        when(this.facturaRepository.findAll()).thenReturn(Collections.emptyList());
        Assertions.assertThrows(DataNotFoundException.class, ()->this.facturaImplService.listaFacturas());
    }
    
    
    @Test
    void when_eliminarFactura_then_retornaVacio() {
        doNothing().when(this.facturaRepository).deleteById(1L);
        this.facturaImplService.eliminarFactura(1L);
    }
    
    @Test
    void when_eliminarFactura_then_retornaBadRequestException() {
        doThrow(new BadRequestException("error")).when(this.facturaRepository).deleteById(1L);
        Assertions.assertThrows(BadRequestException.class, ()-> this.facturaImplService.eliminarFactura(1L));
    }
    
 
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private ConsultarDetalleFacturaPeticionDto mapearPeticionConsultarDetalleFactura()
    {
        ConsultarDetalleFacturaPeticionDto peticionConsultar = new ConsultarDetalleFacturaPeticionDto();
        peticionConsultar.setIdMovimiento(1L);
        peticionConsultar.setIdProducto(1L);
        return peticionConsultar;
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private List<DetalleFactura> mapearDetalleFactura()
    {
        List<DetalleFactura> detalles = new ArrayList<>();
        DetalleFactura detalle = new DetalleFactura();
        detalle.setId(1L);
        detalle.setCantidad(20);
        detalle.setDescripcion("Descripcion 2");
        detalle.setFactura(Factura.builder().id(2L).numeroFactura("02").tipoMovimiento(TipoMovimiento.builder().id(2L).nombre("Compra").build()).build());
        detalle.setFecha(LocalDate.now());
        detalle.setProducto(Producto.builder().id(2L).unidad(Unidad.builder().id(1L).nombre("Unidad").build()).build());
        detalles.add(detalle);
        
        detalle = new DetalleFactura();
        detalle.setId(1L);
        detalle.setCantidad(20);
        detalle.setDescripcion("Descripcion 1");
        detalle.setFactura(Factura.builder().id(1L).numeroFactura("01").tipoMovimiento(TipoMovimiento.builder().id(3L).nombre("Venta").build()).build());
        detalle.setFecha(LocalDate.now());
        detalle.setProducto(Producto.builder().id(1L).unidad(Unidad.builder().id(1L).nombre("Unidad").build()).build());
        detalles.add(detalle);
        
        return detalles;
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private CrearFacturaPeticionDto mapearPeticionCrearfactura()
    {
        CrearFacturaPeticionDto peticion = new CrearFacturaPeticionDto();
        peticion.setFechaCreacion(LocalDate.now());
        peticion.setIdProveedor(1L);
        peticion.setIdTipoMovimiento(1L);
        peticion.setNumeroFactura("001");
        
        List<CrearDetalleFacturaDto> detalles = new ArrayList<>();
        CrearDetalleFacturaDto detalle = new CrearDetalleFacturaDto();
        detalle.setCantidad(20);
        detalle.setDescripcion("Descripcion 2");
        detalle.setIdProducto(2L);
        detalle.setValorTotal(10.0);
        detalle.setValorUnitario(10.0);
        detalles.add(detalle);
        
        peticion.setDetalles(detalles);
        return peticion;
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private List<Factura> mapearListadofactura()
    {
        
        List<Factura> facturas = new ArrayList<>();
        Set<DetalleFactura> detalleFactura = new HashSet<>();
        DetalleFactura detalle = DetalleFactura.builder().id(1L).cantidad(20).descripcion("Compra 20 unidade").fecha(LocalDate.now())
                .producto(Producto.builder().id(1L).cantidad(20).categoria(Categoria.builder().id(1L).nombre("CPU").build()).build())
                .valorTotal(3600.0)
                .valorUnitario(1200.0)
                .build();
        detalleFactura.add(detalle);
        Factura factura = Factura.builder().id(1L).detalleFactura(detalleFactura)
                .fechaCreacion(LocalDate.now())
                .numeroFactura("001")
                .proveedor(Usuario.builder().id(1L).nombreCompleto("ACER S.A.S").build())
                .tipoMovimiento(TipoMovimiento.builder().id(1L).nombre("Compra").build())
                .build();
        facturas.add(factura);
  
        return facturas;
    }
    
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private Set<DetalleFactura> mapearDetalleFacturaCuandoCrearFactura(Factura factura)
    {
        Set<DetalleFactura> detalles = new HashSet<>();
        DetalleFactura detalle = new DetalleFactura();
        detalle.setId(1L);
        detalle.setCantidad(20);
        detalle.setDescripcion("Descripcion 2");
        detalle.setFactura(factura);
        detalle.setFecha(LocalDate.now());
        detalle.setProducto(this.producto);
        detalles.add(detalle);
        return detalles;
    }
}
