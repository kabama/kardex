package com.kbm.kardex.domain.service.inventario.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.kbm.kardex.TestConfig;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.inventario.dto.UnidadDto;
import com.kbm.kardex.infrastucture.entities.inventario.Unidad;
import com.kbm.kardex.infrastucture.repository.inventario.UnidadRepository;

/**
 * Clase con las pruebas unitarias de {@link UnidadImplService}
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@TestConfig
class UnidadImplServiceTest
{
    @Mock
    private UnidadRepository unidadRepository;
    
    @InjectMocks
    private UnidadImplService unidadImplService;
    
    @Test
    void when_consultarUnidades_then_retornamosUnaListaDeUnidades()
    {
        List<Unidad> unidades = new ArrayList<>();
        Unidad unidad = new Unidad();
        unidad.setId(1L);
        unidad.setNombre("Kg");
        unidad.setDescripcion("Kg");
        unidades.add(unidad);
        when(this.unidadRepository.findAll()).thenReturn(unidades);
        List<UnidadDto> respuestaUnidadesService = this.unidadImplService.consultarUnidades();
        assertThat(respuestaUnidadesService).isNotEmpty();
    }
    
    @Test
    void when_consultarUnidades_then_retornamosUnaListaNotFoundException()
    {
        when(this.unidadRepository.findAll()).thenReturn(Collections.emptyList());
        Assertions.assertThrows(DataNotFoundException.class, () -> this.unidadImplService.consultarUnidades());
        
    }
}
