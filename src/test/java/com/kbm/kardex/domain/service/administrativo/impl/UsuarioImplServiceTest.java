package com.kbm.kardex.domain.service.administrativo.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.kbm.kardex.TestConfig;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreatePeticionDto;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreateRespuestaDto;
import com.kbm.kardex.infrastucture.entities.administrativo.Rol;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.repository.administrativo.RolRepository;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;

@TestConfig
class UsuarioImplServiceTest
{
    @Mock
    private UsuarioRepository usuarioRepository;
    
    @Mock
    private RolRepository rolRepository;
    
    @Mock
    private PasswordEncoder bPasswordEnconder;
    
    @InjectMocks
    private UsuarioImplService usuarioImplService;
    
    @Test
    void when_createUsuario_then_retornaUsuario()
    {
        Rol rol = Rol.builder().id(1L).nombre("ADMIN").build();
        UsuarioCreatePeticionDto registrar = mapearUsuarioCreateDto();
        
        when(rolRepository.findById(1L)).thenReturn(Optional.of(rol));
        Set<Rol> roles = new HashSet<>();
        roles.add(rol);
        Usuario usuario = Usuario.builder().email(registrar.getEmail()).roles(roles).nombreCompleto(registrar.getNombreCompleto()).password(bPasswordEnconder.encode(registrar.getPassword())).build();
        when(usuarioRepository.save(usuario)).thenReturn(usuario);
        UsuarioCreateRespuestaDto usuarioCreado = this.usuarioImplService.createUsuario(registrar);
        assertThat(usuarioCreado).isNotNull();
    }
    
    
    @Test
    void when_createUsuario_then_retornaNotFoundException()
    {
        Rol rol = Rol.builder().id(1L).nombre("ADMIN").build();
        UsuarioCreatePeticionDto registrar = mapearUsuarioCreateDto();
        
        when(rolRepository.findById(1L)).thenReturn(Optional.empty());
        Set<Rol> roles = new HashSet<>();
        roles.add(rol);
        Usuario usuario = Usuario.builder().email(registrar.getEmail()).roles(roles).nombreCompleto(registrar.getNombreCompleto()).password(bPasswordEnconder.encode(registrar.getPassword())).build();
        when(usuarioRepository.save(usuario)).thenReturn(usuario);
        Assertions.assertThrows(DataNotFoundException.class, () ->  this.usuarioImplService.createUsuario(registrar));
    }
    
    /**
     * Metodo para mapear los datos para la creacion de un usuario
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/25
     * @since 0.0.1 2020/10/25
     * @return
     */
    private UsuarioCreatePeticionDto mapearUsuarioCreateDto()
    {
        UsuarioCreatePeticionDto user = new UsuarioCreatePeticionDto();
        user.setEmail("admin@mail.com");
        user.setIdRol(1L);
        user.setNombreCompleto("AdminUser");
        user.setPassword("123");
        return user;
    }
}
