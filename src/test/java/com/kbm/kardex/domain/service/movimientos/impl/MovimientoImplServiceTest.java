package com.kbm.kardex.domain.service.movimientos.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.kbm.kardex.TestConfig;
import com.kbm.kardex.commons.enums.StatusModelo;
import com.kbm.kardex.domain.exceptions.ConflictException;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearDetalleMovimientoDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.DetalleMovimientoKardexDto;
import com.kbm.kardex.domain.models.movimientos.dto.enumerador.TipoMovimientoEnum;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.entities.inventario.Categoria;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;
import com.kbm.kardex.infrastucture.entities.inventario.Unidad;
import com.kbm.kardex.infrastucture.entities.movimientos.DetalleMovimiento;
import com.kbm.kardex.infrastucture.entities.movimientos.Movimiento;
import com.kbm.kardex.infrastucture.entities.movimientos.TipoMovimiento;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;
import com.kbm.kardex.infrastucture.repository.inventario.ProductoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.DetalleFacturaRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.DetalleMovimientoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.MovimientoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.TipoMovimientoRepository;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@TestConfig
class MovimientoImplServiceTest
{
    @Mock
    private MovimientoRepository movimientoRepository;
    
    @Mock
    private DetalleMovimientoRepository detalleMovimientoRepository;
    
    @Mock
    private TipoMovimientoRepository tipoMovimientoRepository;
    
    @Mock
    private ProductoRepository productoRepository;
    
    @Mock
    private UsuarioRepository usuarioRepository;
    
    @Mock
    private DetalleFacturaRepository detallefacturaRepository;
    
    @InjectMocks
    private MovimientoImplService movimientoImplService;
    
    @Test
    void when_crearDetalleMovimiento_then_TipoMovimientoInventarioIncial()
    {
        
        CrearDetalleMovimientoDto peticionCrearDetalle = mapearPeticionCrearDetalleMovimiento();
        when(detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(peticionCrearDetalle.getIdMovimiento())).thenReturn(null);
        
        Movimiento movimiento = mapearMovimiento();
        movimiento.setDetalleMovimiento(new HashSet<>());
        when(this.movimientoRepository.findById(peticionCrearDetalle.getIdMovimiento())).thenReturn(Optional.of(movimiento));
        TipoMovimiento tipoMovimiento = mapearTipoMovimiento();
        when(this.tipoMovimientoRepository.findById(peticionCrearDetalle.getTipoMovimientoId())).thenReturn(Optional.of(tipoMovimiento));
        
        when(this.movimientoRepository.save(movimiento)).thenReturn(movimiento);
        doNothing().when(detallefacturaRepository).actualizarEstado(StatusModelo.INACTIVO.getStatus(), peticionCrearDetalle.getIdDetalleFactura());
        
        DetalleMovimientoKardexDto respuestaDetalle = this.movimientoImplService.crearDetalle(peticionCrearDetalle);
        assertThat(respuestaDetalle).isNotNull();
        
        assertThat(respuestaDetalle.getCantidadSaldo()).isEqualTo(peticionCrearDetalle.getCantidad());
        assertThat(respuestaDetalle.getValorSaldo()).isEqualTo(peticionCrearDetalle.getValor());
    }
    
    @Test
    void when_crearDetalleMovimiento_then_TipoMovimientoCompra()
    {
        Movimiento movimiento = mapearMovimiento();
        movimiento.setDetalleMovimiento(new HashSet<>());
        TipoMovimiento tipoMovimiento = mapearTipoMovimiento();
        
        CrearDetalleMovimientoDto peticionCrearDetalle = mapearPeticionCrearDetalleMovimiento();
        peticionCrearDetalle.setTipoMovimientoId(TipoMovimientoEnum.COMPRA.getTipo());
        
        DetalleMovimiento ultimoDetalle = mapearUltimoDetalle();
        
        when(detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(peticionCrearDetalle.getIdMovimiento())).thenReturn(ultimoDetalle);
        when(this.movimientoRepository.findById(peticionCrearDetalle.getIdMovimiento())).thenReturn(Optional.of(movimiento));
        when(this.tipoMovimientoRepository.findById(peticionCrearDetalle.getTipoMovimientoId())).thenReturn(Optional.of(tipoMovimiento));
        when(this.movimientoRepository.save(movimiento)).thenReturn(movimiento);
        doNothing().when(detallefacturaRepository).actualizarEstado(StatusModelo.INACTIVO.getStatus(), peticionCrearDetalle.getIdDetalleFactura());
        
        DetalleMovimientoKardexDto respuestaDetalle = this.movimientoImplService.crearDetalle(peticionCrearDetalle);
        assertThat(respuestaDetalle).isNotNull();
        
        Double resValor = peticionCrearDetalle.getValorUnitario() * peticionCrearDetalle.getCantidad();
        assertThat(respuestaDetalle.getCantidadSaldo()).isEqualTo(50);
        assertThat(respuestaDetalle.getValorSaldo()).isEqualTo((resValor + ultimoDetalle.getValorSaldo()));
    }
    
    @Test
    void when_crearDetalleMovimiento_then_TipoMovimientoVenta()
    {
        Movimiento movimiento = mapearMovimiento();
        movimiento.setDetalleMovimiento(new HashSet<>());
        TipoMovimiento tipoMovimiento = mapearTipoMovimiento();
        
        CrearDetalleMovimientoDto peticionCrearDetalle = mapearPeticionCrearDetalleMovimiento();
        peticionCrearDetalle.setTipoMovimientoId(TipoMovimientoEnum.VENTA.getTipo());
        
        DetalleMovimiento ultimoDetalle = mapearUltimoDetalle();
        
        when(detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(peticionCrearDetalle.getIdMovimiento())).thenReturn(ultimoDetalle);
        when(this.movimientoRepository.findById(peticionCrearDetalle.getIdMovimiento())).thenReturn(Optional.of(movimiento));
        when(this.tipoMovimientoRepository.findById(peticionCrearDetalle.getTipoMovimientoId())).thenReturn(Optional.of(tipoMovimiento));
        when(this.movimientoRepository.save(movimiento)).thenReturn(movimiento);
        doNothing().when(detallefacturaRepository).actualizarEstado(StatusModelo.INACTIVO.getStatus(), peticionCrearDetalle.getIdDetalleFactura());
        DetalleMovimientoKardexDto respuestaDetalle = this.movimientoImplService.crearDetalle(peticionCrearDetalle);
        assertThat(respuestaDetalle).isNotNull();
        
        Double resValor = peticionCrearDetalle.getValorUnitario() * peticionCrearDetalle.getCantidad();
        assertThat(respuestaDetalle.getCantidadSaldo()).isEqualTo((ultimoDetalle.getCantidadSaldo() - peticionCrearDetalle.getCantidad()));
        assertThat(respuestaDetalle.getValorSaldo()).isEqualTo((ultimoDetalle.getValorSaldo() - resValor));
    }
    
    @Test
    void when_crearDetalleMovimiento_then_TipoMovimientoDevolucionCompra()
    {
        Movimiento movimiento = mapearMovimiento();
        movimiento.setDetalleMovimiento(new HashSet<>());
        TipoMovimiento tipoMovimiento = mapearTipoMovimiento();
        
        CrearDetalleMovimientoDto peticionCrearDetalle = mapearPeticionCrearDetalleMovimiento();
        peticionCrearDetalle.setTipoMovimientoId(TipoMovimientoEnum.DEVOLUCION_COMPRA.getTipo());
        
        DetalleMovimiento ultimoDetalle = mapearUltimoDetalle();
        
        when(detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(peticionCrearDetalle.getIdMovimiento())).thenReturn(ultimoDetalle);
        when(this.movimientoRepository.findById(peticionCrearDetalle.getIdMovimiento())).thenReturn(Optional.of(movimiento));
        when(this.tipoMovimientoRepository.findById(peticionCrearDetalle.getTipoMovimientoId())).thenReturn(Optional.of(tipoMovimiento));
        when(this.movimientoRepository.save(movimiento)).thenReturn(movimiento);
        doNothing().when(detallefacturaRepository).actualizarEstado(StatusModelo.INACTIVO.getStatus(), peticionCrearDetalle.getIdDetalleFactura());
        DetalleMovimientoKardexDto respuestaDetalle = this.movimientoImplService.crearDetalle(peticionCrearDetalle);
        assertThat(respuestaDetalle).isNotNull();
        
        Double resValor = peticionCrearDetalle.getValorUnitario() * peticionCrearDetalle.getCantidad();
        assertThat(respuestaDetalle.getCantidadSaldo()).isEqualTo((ultimoDetalle.getCantidadSaldo() - peticionCrearDetalle.getCantidad()));
        assertThat(respuestaDetalle.getValorSaldo()).isEqualTo((ultimoDetalle.getValorSaldo() - resValor));
    }
    
    @Test
    void when_crearDetalleMovimiento_then_TipoMovimientoDevolucionVenta()
    {
        Movimiento movimiento = mapearMovimiento();
        movimiento.setDetalleMovimiento(new HashSet<>());
        TipoMovimiento tipoMovimiento = mapearTipoMovimiento();
        
        CrearDetalleMovimientoDto peticionCrearDetalle = mapearPeticionCrearDetalleMovimiento();
        peticionCrearDetalle.setTipoMovimientoId(TipoMovimientoEnum.DEVOLUCION_VENTA.getTipo());
        
        DetalleMovimiento ultimoDetalle = mapearUltimoDetalle();
        
        when(detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(peticionCrearDetalle.getIdMovimiento())).thenReturn(ultimoDetalle);
        when(this.movimientoRepository.findById(peticionCrearDetalle.getIdMovimiento())).thenReturn(Optional.of(movimiento));
        when(this.tipoMovimientoRepository.findById(peticionCrearDetalle.getTipoMovimientoId())).thenReturn(Optional.of(tipoMovimiento));
        when(this.movimientoRepository.save(movimiento)).thenReturn(movimiento);
        doNothing().when(detallefacturaRepository).actualizarEstado(StatusModelo.INACTIVO.getStatus(), peticionCrearDetalle.getIdDetalleFactura());
        DetalleMovimientoKardexDto respuestaDetalle = this.movimientoImplService.crearDetalle(peticionCrearDetalle);
        assertThat(respuestaDetalle).isNotNull();
        
        Double resValor = peticionCrearDetalle.getValorUnitario() * peticionCrearDetalle.getCantidad();
        assertThat(respuestaDetalle.getCantidadSaldo()).isEqualTo(50);
        assertThat(respuestaDetalle.getValorSaldo()).isEqualTo((ultimoDetalle.getValorSaldo() + resValor));
    }
    
    @Test
    void when_crearDetalleMovimientoUnNuevoDetalle_then_TipoMovimientoDevolucionVenta()
    {
        Movimiento movimiento = mapearMovimiento();
        Set<DetalleMovimiento> detalles = new HashSet<>();
        DetalleMovimiento detalle = DetalleMovimiento.builder().id(1L).cantidad(10).cantidadSaldo(10).descripcion("Compra").build();
        detalles.add(detalle);
        movimiento.setDetalleMovimiento(detalles);
        TipoMovimiento tipoMovimiento = mapearTipoMovimiento();
        
        CrearDetalleMovimientoDto peticionCrearDetalle = mapearPeticionCrearDetalleMovimiento();
        peticionCrearDetalle.setTipoMovimientoId(TipoMovimientoEnum.COMPRA.getTipo());
        
        DetalleMovimiento ultimoDetalle = mapearUltimoDetalle();
        
        when(detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(peticionCrearDetalle.getIdMovimiento())).thenReturn(ultimoDetalle);
        when(this.movimientoRepository.findById(peticionCrearDetalle.getIdMovimiento())).thenReturn(Optional.of(movimiento));
        when(this.tipoMovimientoRepository.findById(peticionCrearDetalle.getTipoMovimientoId())).thenReturn(Optional.of(tipoMovimiento));
        when(this.movimientoRepository.save(movimiento)).thenReturn(movimiento);
        doNothing().when(detallefacturaRepository).actualizarEstado(StatusModelo.INACTIVO.getStatus(), peticionCrearDetalle.getIdDetalleFactura());
        DetalleMovimientoKardexDto respuestaDetalle = this.movimientoImplService.crearDetalle(peticionCrearDetalle);
        assertThat(respuestaDetalle).isNotNull();
        
        Double resValor = peticionCrearDetalle.getValorUnitario() * peticionCrearDetalle.getCantidad();
        assertThat(respuestaDetalle.getCantidadSaldo()).isEqualTo(50);
        assertThat(respuestaDetalle.getValorSaldo()).isEqualTo((ultimoDetalle.getValorSaldo() + resValor));
    }
    
    @Test
    void when_crearDetalleMovimiento_then_ConflictException()
    {
        Assertions.assertThrows(ConflictException.class, () -> this.movimientoImplService.crearDetalle(null));
    }
    
    @Test
    void when_crearMovimiento_retornaDatosNuevoMovimiento()
    {
        Movimiento movimiento = mapearMovimiento();
        
        CrearMovimientoPeticionDto peticionCrear = new CrearMovimientoPeticionDto();
        peticionCrear.setMaximo(50);
        peticionCrear.setMinimo(10);
        peticionCrear.setObservaciones("Nuevo Kardex Producto 1");
        peticionCrear.setProductoId(1L);
        peticionCrear.setUsuarioId(1L);
        
        when(this.productoRepository.findById(peticionCrear.getProductoId())).thenReturn(Optional.of(mapearProducto()));
        when(this.usuarioRepository.findById(peticionCrear.getUsuarioId())).thenReturn(Optional.of(mapearUsuario()));
        when(this.movimientoRepository.save(mapearMovimiento())).thenReturn(movimiento);
        
        CrearMovimientoRespuestaDto respuestaMovimiento = this.movimientoImplService.crearMovimiento(peticionCrear);
        assertThat(respuestaMovimiento).isNotNull();
        
    }
    
    @Test
    void when_crearMovimiento_retornaConflictExceptionNoExisteProducto()
    {
        Movimiento movimiento = mapearMovimiento();
        
        CrearMovimientoPeticionDto peticionCrear = new CrearMovimientoPeticionDto();
        peticionCrear.setMaximo(50);
        peticionCrear.setMinimo(10);
        peticionCrear.setObservaciones("Nuevo Kardex Producto 1");
        peticionCrear.setProductoId(1L);
        peticionCrear.setUsuarioId(1L);
        
        when(this.productoRepository.findById(peticionCrear.getProductoId())).thenReturn(Optional.empty());
        when(this.usuarioRepository.findById(peticionCrear.getUsuarioId())).thenReturn(Optional.of(mapearUsuario()));
        when(this.movimientoRepository.save(mapearMovimiento())).thenReturn(movimiento);
        
        Assertions.assertThrows(ConflictException.class, () -> this.movimientoImplService.crearMovimiento(peticionCrear));
        
    }
    
    @Test
    void when_crearMovimiento_retornaConflictExceptionNoExisteUsuario()
    {
        Movimiento movimiento = mapearMovimiento();
        
        CrearMovimientoPeticionDto peticionCrear = new CrearMovimientoPeticionDto();
        peticionCrear.setMaximo(50);
        peticionCrear.setMinimo(10);
        peticionCrear.setObservaciones("Nuevo Kardex Producto 1");
        peticionCrear.setProductoId(1L);
        peticionCrear.setUsuarioId(1L);
        Producto producto = mapearProducto();
        when(this.productoRepository.findById(peticionCrear.getProductoId())).thenReturn(Optional.of(producto));
        when(this.usuarioRepository.findById(peticionCrear.getUsuarioId())).thenReturn(Optional.empty());
        when(this.movimientoRepository.save(mapearMovimiento())).thenReturn(movimiento);
        
        Assertions.assertThrows(ConflictException.class, () -> this.movimientoImplService.crearMovimiento(peticionCrear));
        
    }
    
    @Test
    void when_buscarMovimientoPorIdTipoMovimientoVenta_then_retornaConsultarMovimientoRespuestaDto()
    {
        Movimiento movimiento = mapearMovimiento();
        movimiento.setProducto(mapearProducto());
        DetalleMovimiento detalle = mapearUltimoDetalle();
        detalle.setTipoMovimiento(TipoMovimiento.builder().id(TipoMovimientoEnum.VENTA.getTipo()).build()); 
        detalle.setMovimiento(movimiento);
        Set<DetalleMovimiento> detalleMovimientos = new HashSet<>();
        detalleMovimientos.add(detalle);
        movimiento.setDetalleMovimiento(detalleMovimientos);
        when(movimientoRepository.findById(1L)).thenReturn(Optional.of(movimiento));
        
        ConsultarMovimientoRespuestaDto respuesta = this.movimientoImplService.buscarMovimientoPorId(1L);
        assertThat(respuesta).isNotNull();
    }
    
    @Test
    void when_buscarMovimientoPorIdTipoMovimientoCompra_then_retornaConsultarMovimientoRespuestaDto()
    {
        Movimiento movimiento = mapearMovimiento();
        movimiento.setProducto(mapearProducto());
        DetalleMovimiento detalle = mapearUltimoDetalle();
        detalle.setTipoMovimiento(TipoMovimiento.builder().id(TipoMovimientoEnum.COMPRA.getTipo()).build()); 
        detalle.setMovimiento(movimiento);
        Set<DetalleMovimiento> detalleMovimientos = new HashSet<>();
        detalleMovimientos.add(detalle);
        movimiento.setDetalleMovimiento(detalleMovimientos);
        when(movimientoRepository.findById(1L)).thenReturn(Optional.of(movimiento));
        
        ConsultarMovimientoRespuestaDto respuesta = this.movimientoImplService.buscarMovimientoPorId(1L);
        assertThat(respuesta).isNotNull();
    }
    
    @Test
    void when_buscarMovimientoPorIdTipoMovimientoCompra_then_retornaDataNotFoundException()
    {
  
        when(movimientoRepository.findById(1L)).thenThrow(new DataNotFoundException(""));
        
        Assertions.assertThrows(DataNotFoundException.class, () ->  this.movimientoImplService.buscarMovimientoPorId(1L));
      
    }
    
    
    @Test
    void when_buscarMovimientoBuscarMovimientoPorIdProducto_then_retornaConsultarMovimientoRespuestaDto()
    {
        Movimiento movimiento = mapearMovimiento();
        movimiento.setUsuario(Usuario.builder().nombreCompleto("Juan Gamez").build());
 
        
        movimiento.setProducto(mapearProducto());
        DetalleMovimiento detalle = mapearUltimoDetalle();
        detalle.setTipoMovimiento(TipoMovimiento.builder().id(TipoMovimientoEnum.COMPRA.getTipo()).build()); 
        detalle.setMovimiento(movimiento);
        Set<DetalleMovimiento> detalleMovimientos = new HashSet<>();
        detalleMovimientos.add(detalle);
        movimiento.setDetalleMovimiento(detalleMovimientos);
        when(movimientoRepository.findByProductoIdOrderByIdAsc(1L)).thenReturn(Optional.of(movimiento));
        
        ConsultarMovimientoRespuestaDto respuesta = this.movimientoImplService.buscarMovimientoPorIdProducto(1L);
        assertThat(respuesta).isNotNull();
    }
    
    @Test
    void when_buscarMovimientobuscarMovimientoPorIdProducto_then_retornaDataNotFoundException()
    {
  
        when(movimientoRepository.findByProductoIdOrderByIdAsc(1L)).thenThrow(new DataNotFoundException(""));
        
        Assertions.assertThrows(DataNotFoundException.class, () ->  this.movimientoImplService.buscarMovimientoPorIdProducto(1L));
      
    }
    
    
    /**
     * Metodo para mapear la creacion de un detalle de movimiento
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return Dto con datos maepados
     */
    private CrearDetalleMovimientoDto mapearPeticionCrearDetalleMovimiento()
    {
        CrearDetalleMovimientoDto detalle = new CrearDetalleMovimientoDto();
        detalle.setCantidad(20);
        detalle.setValorUnitario(12.000);
        detalle.setIdMovimiento(1L);
        detalle.setTipoMovimientoId(1L);
        return detalle;
    }
    
    /**
     * Metodo para el mapeo de un movimiento
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return
     */
    private Movimiento mapearMovimiento()
    {
        Producto producto = Producto.builder().id(1L).cantidad(20).categoria(Categoria.builder().id(1L).nombre("Categoria1").build()).codigo("001").costo(30.000).descripcion("")
                .localizacion("bodega 1").lote("L1").marca("Marca 1").modelo("M1").nombre("Mi Producto 1").observacion("Sin Observacion").build();
        
        return Movimiento.builder().id(1L).fecha(LocalDate.now()).maximo(40).minimo(100).observaciones("Nuevo Kardex Producto 1").producto(producto).build();
    }
    
    /**
     * Metodo mock para un tipo de movimiento
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return
     */
    private TipoMovimiento mapearTipoMovimiento()
    {
        
        return TipoMovimiento.builder().id(1L).nombre("INVENTARIO INICIAL").build();
    }
    
    /**
     * Mock de detallemovimiento
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return
     */
    private DetalleMovimiento mapearUltimoDetalle()
    {
        DetalleMovimiento detalle = new DetalleMovimiento();
        detalle.setCantidadSaldo(30);
        detalle.setValorSaldo(36.000);
        return detalle;
    }
    
    /**
     * Mock para un nuevo producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return
     */
    private Producto mapearProducto()
    {
        
        return Producto.builder().id(1L).nombre("Zapato A").localizacion("Bodega 1").descripcion("Sin descripcion").cantidad(30).codigo("Z-001").unidad(Unidad.builder().nombre("Unidades").build())
                .categoria(Categoria.builder().id(1L).nombre("Categoria 1").build()).build();
    }
    
    /**
     * Mock con datos de un usuario
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return
     */
    private Usuario mapearUsuario()
    {
        return Usuario.builder().email("usuario1@mail.com").id(1L).nombreCompleto("Juan Acevedo").build();
    }
}
