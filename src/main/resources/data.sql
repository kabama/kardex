INSERT INTO unidad(nombre, descripcion) VALUES('Metro', 'Metro');
INSERT INTO unidad(nombre, descripcion) VALUES('Unidad', 'Unidad');
INSERT INTO unidad(nombre, descripcion) VALUES('Kilo', 'Kilo');
INSERT INTO unidad(nombre, descripcion) VALUES('Libra', 'Libra');

INSERT INTO categoria(nombre, descripcion) VALUES('Equipos de computo', 'Equipos de computo');
INSERT INTO categoria(nombre, descripcion) VALUES('Ropa de vestir', 'Ropa de vestir');
INSERT INTO categoria(nombre, descripcion) VALUES('Calzado', 'Calzado');
INSERT INTO categoria(nombre, descripcion) VALUES('Bolsos', 'Bolsos');


INSERT INTO tipo_movimiento(nombre, descripcion) VALUES('Inventario Inicial', 'Inventario Inicial');
INSERT INTO tipo_movimiento(nombre, descripcion) VALUES('Compra', 'Compra');
INSERT INTO tipo_movimiento(nombre, descripcion) VALUES('Venta', 'Venta');
INSERT INTO tipo_movimiento(nombre, descripcion) VALUES('Dev. Compra', 'Dev. Compra');
INSERT INTO tipo_movimiento(nombre, descripcion) VALUES('Dev. Venta', 'Dev. Venta');

INSERT INTO rol(descripcion, nombre) VALUES ('ADMIN', 'ADMIN');
INSERT INTO rol(descripcion, nombre) VALUES ('PROVEEDOR', 'PROVEEDOR');


INSERT INTO usuario(email, nombre_completo,password, active) VALUES('marvel@hotmail.com', 'Tiendas Marvel', '$2a$10$ftkt606Pp1fgNWYzWwUeHer4WU.hdznrObf3LjYU2oeg6t1y7lFnK', true);
INSERT INTO usuario(email, nombre_completo,password, active) VALUES('marvelblue@hotmail.com', 'Tiendas Marvel Blue', '$2a$10$ftkt606Pp1fgNWYzWwUeHer4WU.hdznrObf3LjYU2oeg6t1y7lFnK', true);
INSERT INTO usuario(email, nombre_completo,password, active) VALUES('admin@mail.com', 'Administrador', '$2a$10$ftkt606Pp1fgNWYzWwUeHer4WU.hdznrObf3LjYU2oeg6t1y7lFnK', true);

INSERT INTO  USUARIO_ROL(USUARIO_ID, ROL_ID) VALUES(1, 2);
INSERT INTO  USUARIO_ROL(USUARIO_ID, ROL_ID) VALUES(2, 2);	
INSERT INTO  USUARIO_ROL(USUARIO_ID, ROL_ID) VALUES(3, 1);	

INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'A-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Marvel Basic','Disfraz Iron Man Marvel','Disfraz Iron Man Marvel', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(100, 'B-200', 150.00, 'Producto de la mejor calidad','BODEGA 1', 'L-101', 'Marvel', 'Marvel Basic','Disfraz Capitana Marvel','Disfraz Capitana Marvel', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(90, 'C-200', 87.00, 'Producto de la mejor calidad','BODEGA 1', 'L-102', 'Marvel',  'Marvel Basic','Cubrelecho Avengers + Funda Almohada','Cubrelecho Avengers + Funda Almohada', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(70, 'D-200', 120.00, 'Producto de la mejor calidad','BODEGA 1', 'L-103', 'Marvel',  'Marvel Basic', 'Disfraz Spiderman Hombre Araña Músculo','Disfraz Spiderman Hombre Araña Músculo', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'E-200', 80.00, 'Producto de la mejor calidad','BODEGA 1', 'L-104', 'Marvel' ,'Marvel Basic', 'Scooter Junior Spiderman','Scooter Junior Spiderman', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(250, 'F-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-105', 'Marvel','Marvel Basic', 'Sábana Sencilla Marvel','Sábana Sencilla Marvel', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'I-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Llavero Metálico Logo Deadpool Marvel','Llavero Metálico Logo Deadpool Marvel', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'J-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Llavero Metálico Agente Shield Marvel','Llavero Metálico Agente Shield Marvel', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'JA-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','MARVEL LEGENDS WOLVERINE UNMASKED','MARVEL LEGENDS WOLVERINE UNMASKED', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'JB-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Marvel avengers age of ultron ultron 2.0','Marvel avengers age of ultron ultron 2.0', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'JC-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Disfraz Casco Disfraz Thor Helmet Marvel','Disfraz Casco Disfraz Thor Helmet Marvel', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'I-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','MORRAL MARVEL AVENGERS CLASSICO','MORRAL MARVEL AVENGERS CLASSICO', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'I2-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','juego de llaveros de espuma 3d','juego de llaveros de espuma 3d', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'K-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Sleeping Bag Spiderman','Sleeping Bag Spiderman', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'KL-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Marvel Capitán América Sculpted Mini','Marvel Capitán América Sculpted Mini', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'LL-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Marvel trivia box juego de cartas marvel','marvel trivia box juego de cartas marvel', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'SM-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','Marvel legends series ultron','Marvel legends series ultron', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'DR-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Modelo Nuevo','marvel titan hero series marvels falcon','marvel titan hero series marvels falcon', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(300, 'G-200', 53.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Marvel Basic','Marvel Family Puppet Set','Marvel Family Puppet Set', 1, 2);
INSERT INTO producto(CANTIDAD, CODIGO, COSTO, DESCRIPCION,LOCALIZACION, LOTE, MARCA, MODELO, NOMBRE, OBSERVACION, CATEGORIA_ID, UNIDAD_ID) 
VALUES(30, 'H-200', 50.00, 'Producto de la mejor calidad','BODEGA 1', 'L-100', 'Marvel', 'Marvel Basic','Silver Buffalo MC7034 Marvel Comics','Silver Buffalo MC7034 Marvel Comics', 1, 2);






 
