package com.kbm.kardex.application.config;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;

import com.fasterxml.classmate.TypeResolver;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/29
 * @since 0.0.1 2020/10/29
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig
{
    /** Mensaje de error general */
    private static final String MENSAJE_ERROR_GENERAL_500 = "Ocurre cuando falla el sistema al realizar una operacion";
    
    @Bean
    public Docket api()
    {
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
        docket.select().apis(RequestHandlerSelectors.basePackage("com.kbm.kardex.application"))
                
                .paths(PathSelectors.regex("/.*")).build().pathMapping("/").directModelSubstitute(LocalDate.class, String.class).genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(newRule(typeResolver.resolve(DeferredResult.class, typeResolver.resolve(ResponseEntity.class, WildcardType.class)), typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false)
                
                .globalResponseMessage(RequestMethod.GET, newArrayList(new ResponseMessageBuilder().code(500).message(MENSAJE_ERROR_GENERAL_500)
                        // .responseModel(vndErrorsModelRef)
                        .build()))
                .globalResponseMessage(RequestMethod.POST, newArrayList(new ResponseMessageBuilder().code(500).message(MENSAJE_ERROR_GENERAL_500)
                        // .responseModel(vndErrorsModelRef)
                        .build()))
                .globalResponseMessage(RequestMethod.PUT, newArrayList(new ResponseMessageBuilder().code(500).message(MENSAJE_ERROR_GENERAL_500)
                        // .responseModel(vndErrorsModelRef)
                        .build()))
                .globalResponseMessage(RequestMethod.DELETE, newArrayList(new ResponseMessageBuilder().code(500).message(MENSAJE_ERROR_GENERAL_500)
                        // .responseModel(vndErrorsModelRef)
                        .build()))
                .globalResponseMessage(RequestMethod.PATCH, newArrayList(new ResponseMessageBuilder().code(500).message(MENSAJE_ERROR_GENERAL_500)
                        // .responseModel(vndErrorsModelRef)
                        .build()))
                .securityContexts(newArrayList(securityContext())).enableUrlTemplating(true).tags(new Tag("Kardex Portal", "API del Portal Kardex"));
        
        docket.apiInfo(apiInfo());
        docket.globalOperationParameters(crearParamaetrosHeaderCanonico());
        return docket;
    }
    
    private ApiInfo apiInfo()
    {
        return new ApiInfoBuilder().title("Proyecto Kardex").description("Registra los datos para kardex").contact(new Contact("Kaleth Bahena", "url", "kbmplaying@hotmail.com"))
                .version("1.0").license("Apache License Version 2.0").build();
    }
    
    /**
     * Crea parametros con header canonicos
     * @author Kaleth Bahena
     * @version 0.0.1 2018/05/30
     * @since 0.0.1 2018/05/28
     * @return Arreglo con los parametros canonicos
     */
    private ArrayList<Parameter> crearParamaetrosHeaderCanonico()
    {
        ArrayList<Parameter> parametros = new ArrayList<>();
        
        String paramType = "header";
        
        parametros.add(new ParameterBuilder().name("Authorization").description("Header con el JWT. Aplica solo a los servicios que se consumen despues de validar el OTP. Ejemplo: Bearer VALOR_TOKEN")
                .modelRef(new ModelRef("string")).parameterType(paramType).required(true).build());
        
        return parametros;
    }
    
    @Autowired
    private TypeResolver typeResolver;
    
    private SecurityContext securityContext()
    {
        return SecurityContext.builder().securityReferences(defaultAuth()).forPaths(PathSelectors.regex("/anyPath.*")).build();
    }
    
    List<SecurityReference> defaultAuth()
    {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return newArrayList(new SecurityReference("mykey", authorizationScopes));
    }
    
    @Bean
    SecurityConfiguration security()
    {
        return SecurityConfigurationBuilder.builder().clientId("test-app-client-id").clientSecret("test-app-client-secret").realm("test-app-realm").appName("test-app").scopeSeparator(",")
                .additionalQueryStringParams(null).useBasicAuthenticationWithAccessCodeGrant(false).build();
    }
    
    @Bean
    UiConfiguration uiConfig()
    {
        return UiConfigurationBuilder.builder().deepLinking(true).displayOperationId(false).defaultModelsExpandDepth(1).defaultModelExpandDepth(1).defaultModelRendering(ModelRendering.EXAMPLE)
                .displayRequestDuration(false).docExpansion(DocExpansion.NONE).filter(false).maxDisplayedTags(null).operationsSorter(OperationsSorter.ALPHA).showExtensions(false)
                .tagsSorter(TagsSorter.ALPHA).supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS).validatorUrl(null).build();
    }
    
}
