package com.kbm.kardex.application.inventario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kbm.kardex.domain.models.inventario.dto.ProductoDto;
import com.kbm.kardex.domain.service.inventario.ProductoService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Getter;

@RestController
@RequestMapping("/private/productos")
public class ProductoController
{
    @Autowired
    @Getter
    private ProductoService productoService;
    
    /**
     * Consulta todos los productos
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param marca
     * @return
     */
    @ApiOperation(value = "Consulta el listado de productos", notes = "Retorna datos de los productos")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProductoDto>> findAll()
    {
        List<ProductoDto> productos = productoService.findAll();
        return ResponseEntity.ok(productos);
    }
    
    /**
     * Consulta por marca
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param marca
     * @return
     */
    @ApiOperation(value = "Consulta los datos de los productos por marca", notes = "Restorna datos de los productos")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/marca/{marca}")
    public ResponseEntity<List<ProductoDto>> findByMarca(@PathVariable("marca") String marca)
    {
        List<ProductoDto> productos = productoService.findByMarca(marca);
        return ResponseEntity.ok(productos);
    }
    
    /**
     * Consulta por nombre
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param nombre
     * @return
     */
    @ApiOperation(value = "Consulta los datos de los productos por nombre", notes = "Restorna datos de los productos")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/nombre/{nombre}")
    public ResponseEntity<List<ProductoDto>> findByNombre(@PathVariable("nombre") String nombre)
    {
        List<ProductoDto> productos = productoService.findByNombre(nombre);
        return ResponseEntity.ok(productos);
    }
    
    /**
     * Consulta por codigo de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param codigo
     * @return
     */
    @ApiOperation(value = "Consulta los datos de los productos por codigo", notes = "Restorna datos de los productos")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/codigo/{codigo}")
    public ResponseEntity<List<ProductoDto>> findByCodigo(@PathVariable("codigo") String codigo)
    {
        List<ProductoDto> productos = productoService.findByCodigo(codigo);
        return ResponseEntity.ok(productos);
    }
    
    /**
     * Consulta por codigo de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param codigo
     * @return
     */
    @ApiOperation(value = "Consulta los datos de los productos por id", notes = "Restorna datos de los productos")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
    public ResponseEntity<ProductoDto> findByCodigo(@PathVariable("id") Long id)
    {
        ProductoDto productos = productoService.findById(id);
        return ResponseEntity.ok(productos);
    }
}
