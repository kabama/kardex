package com.kbm.kardex.application.inventario;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kbm.kardex.domain.models.inventario.dto.CategoriaDto;
import com.kbm.kardex.domain.service.inventario.CategoriaService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Getter;

/**
 * Controlador para el manejo de las categorias
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@RestController
@RequestMapping(value = "/private")
public class CategoriaController
{
    
    @Autowired
    @Getter
    private CategoriaService categoriaService;
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @return
     */
    @ApiOperation(value = "Consulta el listado de categorias", notes = "Retorna datos de las categorias")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
    })
    @GetMapping(value = "/categorias", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CategoriaDto>> consultarUnidades()
    {
        List<CategoriaDto> categorias = getCategoriaService().consultarCategorias();
        return ResponseEntity.ok(categorias);
    }
}
