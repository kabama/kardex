package com.kbm.kardex.application.movimientos;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kbm.kardex.domain.models.movimientos.dto.ConsultarMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearDetalleMovimientoDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.DetalleMovimientoKardexDto;
import com.kbm.kardex.domain.models.movimientos.dto.TipoMovimientoDto;
import com.kbm.kardex.domain.service.movimientos.MovimientoService;

import lombok.Getter;

/**
 * Clase controladora para el manejo de los movimientos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@RestController
@RequestMapping(MovimientoController.URL_BASE)
@Validated
public class MovimientoController
{
    protected static final String URL_BASE = "/private/movimientos";
    
    @Autowired
    @Getter
    private MovimientoService movimientoService;
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param peticionCrear
     * @return
     */
    @PostMapping(value = "/crear-movimiento", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CrearMovimientoRespuestaDto> crearMovimiento(@RequestBody @Valid CrearMovimientoPeticionDto peticionCrear)
    {
        CrearMovimientoRespuestaDto respuestaCrear = getMovimientoService().crearMovimiento(peticionCrear);
        return ResponseEntity.ok(respuestaCrear);
    }
    
    @PutMapping(value = "/crear-detalle", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DetalleMovimientoKardexDto> crearDetalle(@RequestBody @Valid CrearDetalleMovimientoDto peticion)
    {
        DetalleMovimientoKardexDto respuestaCrearDetalle = getMovimientoService().crearDetalle(peticion);
        return ResponseEntity.ok(respuestaCrearDetalle);
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param idMovimiento
     * @return
     */
    @GetMapping(value = "/consultar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ConsultarMovimientoRespuestaDto> buscarMovimientoPorId(@PathVariable("id") Long idMovimiento)
    {
        ConsultarMovimientoRespuestaDto respuestaConsultar = getMovimientoService().buscarMovimientoPorId(idMovimiento);
        return ResponseEntity.ok(respuestaConsultar);
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param idMovimiento
     * @return
     */
    @GetMapping(value = "/consultar/producto/{idProducto}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ConsultarMovimientoRespuestaDto> buscarMovimientoPorIdProducto(@PathVariable("idProducto") Long idProducto)
    {
        ConsultarMovimientoRespuestaDto respuestaConsultar = getMovimientoService().buscarMovimientoPorIdProducto(idProducto);
        return ResponseEntity.ok(respuestaConsultar);
    }
    
    @GetMapping(value = "/consultar/tipo-movimiento", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TipoMovimientoDto>> listaDeTipoMovimientos(){
        List<TipoMovimientoDto> respuestaConsultar = getMovimientoService().listaDeTipoMovimientos();
        return ResponseEntity.ok(respuestaConsultar);
    }
    
}
