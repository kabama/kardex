package com.kbm.kardex.application.movimientos;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaRespuestaDto;
import com.kbm.kardex.domain.service.movimientos.FacturaService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Getter;

/**
 * Clase controladora para el manejode las factutas
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/25
 */
@RestController
@RequestMapping("/private/factura")
@Validated
public class FacturaController
{
    
    @Autowired
    @Getter
    private FacturaService facturaService;
    
    /**
     * Busca los detalles de facturas por id de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/25
     * @since 0.0.1 2020/10/25
     * @param idProducto
     * @param idMovimiento
     * @return
     */
    @ApiOperation(value = "Consulta los datos de los detalles de factura por id producto", notes = "Retorna datos de los detalles de factura")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
        @ApiResponse(code = 409, message = "Sucede si en la operación se encuentran errores del cliente"),
    })
    @GetMapping(value = "/detalle/producto/{idProducto}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ConsultarDetalleFacturaRespuestaDto>> findByProductoId(@PathVariable("idProducto") Long idProducto)
    {
        ConsultarDetalleFacturaPeticionDto peticionConsultar = new ConsultarDetalleFacturaPeticionDto();
        peticionConsultar.setIdProducto(idProducto);
        List<ConsultarDetalleFacturaRespuestaDto> respuestConsultar = getFacturaService().findByProductoId(peticionConsultar);
        return ResponseEntity.ok(respuestConsultar);
    }
    
    /**
     * Metodo encargado de crear una factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param peticionCrear
     * @return
     */
    @ApiOperation(value = "Crea una nueva factura", notes = "Retorna datos de la factura creada")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 409, message = "Sucede si en la operación se encuentran errores del cliente"),
    })
    @PostMapping(value = "/crear-factura", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CrearFacturaRespuestaDto> crearFactura(@RequestBody @Valid CrearFacturaPeticionDto peticionCrear)
    {
        CrearFacturaRespuestaDto respuestaCrear = getFacturaService().crearFactura(peticionCrear);
        return ResponseEntity.ok(respuestaCrear); 
    }
    
    
    /**
     * Metodo que elimina una factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param id
     */
    @ApiOperation(value = "Elimina una factura por id", notes = "No hay retorno de valores")
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 409, message = "Sucede si en la operación se encuentran errores del cliente"),
    })
    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<Void> eliminarFactura(@PathVariable("id") Long id)
    {
        getFacturaService().eliminarFactura(id);
        return ResponseEntity.noContent().build();
    }
    
    /**
     * Busca los detalles de facturas por id de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param id
     * @return
     */
    @ApiOperation(value = "Consulta los datos de las factura", notes = "Retorna datos de las factura")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
    })
    @GetMapping(value = "/consultar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CrearFacturaRespuestaDto>> listaFacturas()
    {
        List<CrearFacturaRespuestaDto> listaFacturas = getFacturaService().listaFacturas();
        return ResponseEntity.ok(listaFacturas);
    }
}
