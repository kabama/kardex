package com.kbm.kardex.application.administrativo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kbm.kardex.domain.models.administrativo.dto.ProveedorDto;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreateRespuestaDto;
import com.kbm.kardex.domain.service.administrativo.UsuarioService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Getter;


@RestController
@RequestMapping("/private/usuario")
@Validated
public class UsuarioController
{
    @Autowired
    @Getter
    private UsuarioService usuarioService;
    
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/26
     * @since 0.0.1 2020/10/26
     * @param email
     * @return
     */
    @ApiOperation(value = "Agrega un nuevo usuario", notes = "Retorna datos basicos del usuario creado")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
        @ApiResponse(code = 409, message = "Sucede si en la operación se encuentran errores del cliente"),
    })
    @GetMapping(value = "/consultar/{email}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioCreateRespuestaDto> registrar(@PathVariable("email") String email)
    {
        UsuarioCreateRespuestaDto usuarioRegistrado = getUsuarioService().cargarDatosUsuarioPorEmail(email);
        return ResponseEntity.ok(usuarioRegistrado);
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/26
     * @since 0.0.1 2020/10/26
     * @param email
     * @return
     */
    @ApiOperation(value = "Consulta usuario por rol", notes = "Restorna datos del usuario creado")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no se encuentran datos"),
        @ApiResponse(code = 409, message = "Sucede si en la operación se encuentran errores del cliente"),
    })
    @GetMapping(value = "/consultar/rol/{idRol}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ProveedorDto>> consultarProveedores(@PathVariable("idRol") Long idRol)
    {
        List<ProveedorDto> usuarioRegistrado = getUsuarioService().cargarProveedores(idRol);
        return ResponseEntity.ok(usuarioRegistrado);
    }
}
