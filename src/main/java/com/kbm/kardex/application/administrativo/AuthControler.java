package com.kbm.kardex.application.administrativo;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kbm.kardex.domain.models.administrativo.dto.LoginRequest;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreatePeticionDto;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreateRespuestaDto;
import com.kbm.kardex.domain.service.administrativo.UsuarioService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.Getter;

@RestController
@RequestMapping("/auth")
@Validated
public class AuthControler
{
    
    @Autowired
    @Getter
    private UsuarioService usuarioService;
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/26
     * @since 0.0.1 2020/10/26
     * @param registrar
     * @return
     */
    @ApiOperation(value = "Registra un nuevo usuario", notes = "Retorna datos del usuario creado")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 409, message = "Sucede si en la operación se encuentran errores del cliente"),
    })
    
    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioCreateRespuestaDto> registrar(@RequestBody @Valid UsuarioCreatePeticionDto registrar)
    {
        UsuarioCreateRespuestaDto usuarioRegistrado = getUsuarioService().createUsuario(registrar);
        return ResponseEntity.ok(usuarioRegistrado);
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/26
     * @since 0.0.1 2020/10/26
     * @param registrar
     * @return
     */
    @ApiOperation(value = "Realiza el login usuario", notes = "Retorna datos del usuario")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Sucede si en la operación ha sido exitosa"),
        @ApiResponse(code = 409, message = "Sucede si en la operación se encuentran errores del cliente"),
        @ApiResponse(code = 204, message = "Sucede si en la operación no encuentra datos"),
    })
    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsuarioCreateRespuestaDto> login(@RequestBody LoginRequest loginRequest)
    {
        return ResponseEntity.ok().build();
    }
}
