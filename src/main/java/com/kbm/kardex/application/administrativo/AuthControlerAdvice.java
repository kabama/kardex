package com.kbm.kardex.application.administrativo;

import java.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.kbm.kardex.domain.exceptions.BadRequestException;
import com.kbm.kardex.domain.exceptions.ConflictException;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.exceptions.dto.ErrorMessageDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping(produces = "application/vnd.error+json")
@RestControllerAdvice(assignableTypes = AuthControler.class)
public class AuthControlerAdvice
{
    private static final String ERROR_ADVICE_EXCEPTION_FORMAT = "Error Advicer - {0} - Exception type: {1}";
    
    /**
     * Error data corrupted persistence exception handler response entity.
     * @param conlictException the data corrupted persistence exception
     * @return the response entity
     */
    @ExceptionHandler(value = { ConflictException.class })
    public ResponseEntity<ErrorMessageDto> errorDataCorruptedExceptionHandler(final ConflictException conlictException)
    {
        log.info(ERROR_ADVICE_EXCEPTION_FORMAT, AuthControler.class, conlictException.getClass());
        
        ErrorMessageDto error = ErrorMessageDto.builder().message(conlictException.getMessage()).description(conlictException.getMessage()).statusCode(HttpStatus.CONFLICT.value())
                .timestamp(LocalDate.now()).build();
        return new ResponseEntity<>(error, HttpStatus.CONFLICT);
    }
    
    /**
     * Error data not found persistence exception handler response entity.
     * @param dataNotFoundPersistenceException the data not found persistence exception
     * @return the response entity
     */
    @ExceptionHandler(value = { DataNotFoundException.class })
    public ResponseEntity<ErrorMessageDto> errorDataNotFoundExceptionHandler(final DataNotFoundException dataNotFoundException)
    {
        log.info(ERROR_ADVICE_EXCEPTION_FORMAT, AuthControler.class, dataNotFoundException.getClass());
        ErrorMessageDto error = ErrorMessageDto.builder().message(dataNotFoundException.getMessage()).description(dataNotFoundException.getMessage()).statusCode(HttpStatus.NO_CONTENT.value())
                .timestamp(LocalDate.now()).build();
        return new ResponseEntity<>(error, HttpStatus.NO_CONTENT);
    }
    
    /**
     * Error data corrupted service exception handler response entity.
     * @param dataCorruptedServiceException the data corrupted service exception
     * @return the response entity
     */
    @ExceptionHandler(value = { BadRequestException.class })
    public ResponseEntity<ErrorMessageDto> errorDataCorruptedServiceExceptionHandler(final BadRequestException dataBadRequestException)
    {
        log.info(ERROR_ADVICE_EXCEPTION_FORMAT, AuthControler.class, dataBadRequestException.getClass());
        ErrorMessageDto error = ErrorMessageDto.builder().message(dataBadRequestException.getMessage()).description(dataBadRequestException.getMessage()).statusCode(HttpStatus.NO_CONTENT.value())
                .timestamp(LocalDate.now()).build();
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
