package com.kbm.kardex.auth.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.kbm.kardex.domain.exceptions.JwtException;

@Service
public class JwtService
{
    
    public static final String BEARER        = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    
    private static final String USER                   = "user";
    private static final String NAME                   = "name";
    private static final String ROLES                  = "roles";
    private static final String ISSUER                 = "kbm-kardex";
    private static final int    EXPIRES_IN_MILLISECOND = 3600000;
    private static final String SECRET                 = "secret-password-test";
    
    public String createToken(String user, String name, String[] roles)
    {
        return JWT.create().withIssuer(ISSUER).withIssuedAt(new Date()).withNotBefore(new Date()).withExpiresAt(new Date(System.currentTimeMillis() + EXPIRES_IN_MILLISECOND)).withClaim(USER, user)
                .withClaim(NAME, name).withArrayClaim(ROLES, roles).sign(Algorithm.HMAC256(SECRET));
    }
    
    public boolean isBearer(String authorization)
    {
        return authorization != null && authorization.startsWith(BEARER) && authorization.split("\\.").length == 3;
    }
    
    public String user(String authorization)
    {
        return this.verify(authorization).getClaim(USER).asString();
    }
    
    private DecodedJWT verify(String authorization)
    {
        if (!this.isBearer(authorization))
        {
            throw new JwtException("It is not Bearer");
        }
        try
        {
            return JWT.require(Algorithm.HMAC256(SECRET)).withIssuer(ISSUER).build().verify(authorization.substring(BEARER.length()));
        }
        catch (Exception exception)
        {
            throw new JwtException("JWT is wrong. " + exception.getMessage());
        }
        
    }
    
    public List<String> roles(String authorization)
    {
        return Arrays.asList(this.verify(authorization).getClaim(ROLES).asArray(String.class));
    }
    
    public String create(Authentication auth)
    {
        
        String username = ((User) auth.getPrincipal()).getUsername();
        Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
        List<String> rolesJwt = roles.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        String[] rolesClaims =   rolesJwt.toArray(new String[rolesJwt.size()]);
        
        return JWT.create().withIssuer(ISSUER).withIssuedAt(new Date()).withNotBefore(new Date()).withExpiresAt(new Date(System.currentTimeMillis() + EXPIRES_IN_MILLISECOND)).withClaim(USER, username)
                .withClaim(NAME, username).withArrayClaim(ROLES, rolesClaims).sign(Algorithm.HMAC256(SECRET));
    }
    
}