package com.kbm.kardex.auth;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.json.JsonParseException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kbm.kardex.auth.service.JwtService;
import com.kbm.kardex.domain.models.administrativo.dto.ErrorLoginRequest;
import com.kbm.kardex.domain.models.administrativo.dto.LoginRequest;
import com.kbm.kardex.domain.models.administrativo.dto.LoginResponse;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter
{
    private final AuthenticationManager authenticationManager;
    private JwtService                 jwtService;
    
    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JwtService jwtService)
    {
        this.authenticationManager = authenticationManager;
        setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/auth/login", "POST"));
        this.jwtService = jwtService;
    }
    
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            LoginRequest user = new ObjectMapper().readValue(request.getInputStream(), LoginRequest.class);
            Authentication authToken = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
            return authenticationManager.authenticate(authToken);
        }
        catch (IOException e)
        {
            throw new JsonParseException(e);
        }
        
    }
    
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException
    {
        String token = jwtService.create(authResult); 
       
        List<String> roles = authResult.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        LoginResponse respuesta = new LoginResponse();
        respuesta.setToken(token);
        respuesta.setEmail(authResult.getName());
        respuesta.setRoles(roles);
        response.addHeader(JwtService.HEADER_STRING, JwtService.BEARER + token);
        response.setStatus(200);
        response.setContentType("application/json");
        response.getWriter().write(new ObjectMapper().writeValueAsString(respuesta));
    }
    
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException
    {
        ErrorLoginRequest body = ErrorLoginRequest.builder().message("Error de autenticación: username o password incorrecto!").error(failed.getMessage()).status(HttpStatus.UNAUTHORIZED.value()).build();
        response.getWriter().write(new ObjectMapper().writeValueAsString(body));
        response.setContentType("application/json");
    }
    
}
