package com.kbm.kardex.infrastucture.repository.movimientos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.movimientos.DetalleMovimiento;

/**
 * Respositorio para el manejo de los detalles de movimientos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Repository
public interface DetalleMovimientoRepository extends JpaRepository<DetalleMovimiento, Long>
{
    DetalleMovimiento findTopByMovimientoIdOrderByIdDesc(Long idMovimiento);
}
