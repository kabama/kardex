package com.kbm.kardex.infrastucture.repository.movimientos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.movimientos.Movimiento;

/**
 * Repositorio para el manejo de los movimientos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Repository
public interface MovimientoRepository extends JpaRepository<Movimiento, Long>
{

    /**
     * Consultamos un movimiento por id de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/26
     * @since 0.0.1 2020/10/26
     * @param id
     * @return
     */
    Optional<Movimiento> findByProductoIdOrderByIdAsc(Long id);
    
}
