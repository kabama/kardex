package com.kbm.kardex.infrastucture.repository.inventario;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.inventario.Producto;

/**
 * Repositorio para el manejo de los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/21
 * @since 0.0.1 2020/10/21
 */
@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long>
{
    
    
    /**
     * Consulta por marca 
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param marca
     * @return
     */
    List<Producto> findByMarca(String marca);
    
    
    /**
     * Consulta por nombre
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param nombre
     * @return
     */
    List<Producto> findByNombre(String nombre);
    
    
    /**
     * Consulta por codigo de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param codigo
     * @return
     */
    List<Producto> findByCodigo(String codigo);
    
    
}
