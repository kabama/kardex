package com.kbm.kardex.infrastucture.repository.movimientos;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.movimientos.Factura;

/**
 * Repositorio para el manejo de facturas
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Repository
public interface FacturaRepository extends JpaRepository<Factura, Long>
{
    
    /**
     * Buscar factura por fecha creacion
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param fechaCreacion
     * @return
     */
    Factura findByFechaCreacion(LocalDate fechaCreacion);
    
    /**
     * Buscar factura por numero
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    Factura findByNumeroFactura(String numeroFactura);
    
}
