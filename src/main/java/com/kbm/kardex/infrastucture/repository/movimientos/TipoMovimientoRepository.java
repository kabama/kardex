package com.kbm.kardex.infrastucture.repository.movimientos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.movimientos.TipoMovimiento;

/**
 * Repsoitorio para el manejo de los tipos de movimientos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Repository
public interface TipoMovimientoRepository extends CrudRepository<TipoMovimiento, Long>
{
    
}
