package com.kbm.kardex.infrastucture.repository.inventario;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.inventario.Unidad;

/**
 * Repositorio para el manejo de las unidades
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/21
 * @since 0.0.1 2020/10/21
 */
@Repository
public interface UnidadRepository extends CrudRepository<Unidad, Long>
{
    
}
