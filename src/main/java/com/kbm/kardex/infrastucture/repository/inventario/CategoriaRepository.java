package com.kbm.kardex.infrastucture.repository.inventario;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.inventario.Categoria;


/**
 * Repositorio para el manejo de las categorias de productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/21
 * @since 0.0.1 2020/10/21
 */
@Repository
public interface CategoriaRepository extends CrudRepository<Categoria, Long>
{
    
}
