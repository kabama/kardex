package com.kbm.kardex.infrastucture.repository.administrativo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.administrativo.Rol;

/**
 * Respoitorio para el manejo de roles
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Repository
public interface RolRepository extends CrudRepository<Rol, Long>
{
    
}
