package com.kbm.kardex.infrastucture.repository.administrativo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;

/**
 * Repositorio para el manejo de los usuarios
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>
{
    /**
     * Consulta usuario por email
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/25
     * @since 0.0.1 2020/10/25
     * @param email
     * @return
     */
    Optional<Usuario> findByEmail(String email);
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/26
     * @since 0.0.1 2020/10/26
     * @param rolId
     * @return
     */
    List<Usuario> findByRolesId(Long rolId);
    
}
