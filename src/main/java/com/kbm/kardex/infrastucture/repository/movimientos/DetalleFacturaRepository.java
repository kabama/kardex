package com.kbm.kardex.infrastucture.repository.movimientos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.kbm.kardex.infrastucture.entities.movimientos.DetalleFactura;

/**
 * Repsotorio par el detalle de las facturas
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Repository
public interface DetalleFacturaRepository extends JpaRepository<DetalleFactura, Long>
{
 
    
    /**
     * Busca los detalles de facturas por id de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param id
     * @return
     */
    List<DetalleFactura> findByProductoIdAndStatus(Long id, Long status);
    
    
    @Transactional
    @Modifying
    @Query(value = "update detalle_factura d set d.status = :status where d.id = :id", nativeQuery = true)
    void actualizarEstado(@Param(value = "status") Long status, @Param(value = "id") Long id);
}
