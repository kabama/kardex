package com.kbm.kardex.infrastucture.repository.movimientos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kbm.kardex.infrastucture.entities.movimientos.Balance;

/**
 * Repositorio para el manejo de los balances
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Repository
public interface BalanceRepository extends JpaRepository<Balance, Long>
{
    
}
