package com.kbm.kardex.infrastucture.entities.administrativo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que mapea los roles de usuario
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/21
 * @since 0.0.1 2020/10/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "rol")
public class Rol
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   id;
    @Column(length = 25, nullable = false)
    private String nombre;
    @Column(length = 250, nullable = true)
    private String descripcion;
}
