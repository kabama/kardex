package com.kbm.kardex.infrastucture.entities.movimientos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que mapea los tipo de movimientos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/21
 * @since 0.0.1 2020/10/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "tipo_movimiento")
public class TipoMovimiento
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   id;
    @Column(nullable = false, length = 50)
    private String nombre;
    @Column(nullable = true, length = 250)
    private String descripcion;
}
