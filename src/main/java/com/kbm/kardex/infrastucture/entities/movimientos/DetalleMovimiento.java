package com.kbm.kardex.infrastucture.entities.movimientos;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Clase que mapea los detalles de movimientos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ToString(exclude = {"movimiento" })
@EqualsAndHashCode(exclude = { "movimiento" })
@Entity
@Table(name = "detalle_movimiento")
public class DetalleMovimiento
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long    id;
    
    
    @CreatedDate
    @Column(nullable = false, updatable = false)
    private LocalDate fechaCreacion;
    
    @Column(name = "fecha_movimiento", nullable = false)
    private LocalDate fechaMovimiento;
    
    @Column(nullable = false)
    private String  descripcion;
    
    @Column(nullable = false)
    private Double  valorUnitario;

    @Column(nullable = false)
    private Integer cantidad;
    
    @Column(nullable = false)
    private Double  valor;
    
    /** Campo para manejo de saldos finales del detalle*/
    @Column(nullable = false)
    private Integer cantidadSaldo;
    
    @Column(nullable = false)
    private Double  valorSaldo;

    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "movimiento_id")
    private Movimiento movimiento;
    
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_movimiento_id")
    private TipoMovimiento tipoMovimiento;
    
    @PrePersist
    public void init() {
        this.fechaCreacion = LocalDate.now();
    }

}
