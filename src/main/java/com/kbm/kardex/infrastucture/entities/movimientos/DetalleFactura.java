package com.kbm.kardex.infrastucture.entities.movimientos;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kbm.kardex.commons.enums.StatusModelo;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Clase que mapea las facturas
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ToString(exclude = {"factura", "producto"})
@EqualsAndHashCode(exclude = {"factura", "producto"})
@Entity
@Table(name = "detalle_factura")
public class DetalleFactura
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @CreatedDate
    private LocalDate fecha;
    
    @Min(message = "La cantidad debe ser minimo un producto", value = 1)
    @Column(nullable = false)
    private Integer cantidad;
    
    @NotBlank(message = "La descripcion es obligatoria")
    @Column(nullable = false, length = 250)
    private String descripcion;
    
    @Column(nullable = false, name = "valor_total")
    private Double valorTotal;
    
    @Column(nullable = false, name = "valor_unitario")
    private Double valorUnitario;
    
    @Column(nullable = false)
    private Long status;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "producto_id")
    private Producto producto;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "factura_id")
    private Factura factura;
    
    
    @PrePersist
    public void init() {
        this.fecha = LocalDate.now();
        this.status = StatusModelo.ACTIVO.getStatus();
    }
}
