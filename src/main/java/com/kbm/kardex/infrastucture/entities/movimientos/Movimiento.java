package com.kbm.kardex.infrastucture.entities.movimientos;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Clase que mapea los movimientos hace de kardex
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ToString(exclude = {"detalleMovimiento", "producto"})
@EqualsAndHashCode(exclude = {"detalleMovimiento", "producto"})
@Entity
@Table(name = "movimiento")
public class Movimiento
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @CreatedDate
    private LocalDate fecha;
    
    @Column(length = 200, nullable = false)
    private String observaciones;
    
    @Column(nullable = true)
    private Integer minimo;

    @Column(nullable = true)
    private Integer maximo;
    
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    private Producto producto;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario usuario;
    
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "movimiento", orphanRemoval = true)
    private Set<DetalleMovimiento> detalleMovimiento;
    
    @PrePersist
    public void init() {
        this.fecha = LocalDate.now();
    }
}
