package com.kbm.kardex.infrastucture.entities.movimientos;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que mapea los balances
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "balance")
public class Balance
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long    id;
    @Column(nullable = false, length = 250)
    private String descripcion;
    
    @CreatedDate
    @Column(updatable = false, name = "fecha_creacion")
    private LocalDateTime fechaCreacion;
    
    @LastModifiedBy
    @Column(name = "fecha_modificacion")
    private LocalDateTime fechaModificacion;
    
    /** Primera entrada de un movimiento*/
    @Column(nullable = false)
    private Double valorInicial;

    @Column(nullable = false)
    private Double debe;
    @Column(nullable = false)
    private Double haber;
    /** Sumatoria de detalle de un movimeinto*/
    @Column(nullable = false)
    private Double saldo;

//    @JsonIgnore
//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "balance")
//    private Set<DetalleMovimiento> detalleMovimiento;
}
