package com.kbm.kardex.infrastucture.entities.administrativo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kbm.kardex.infrastucture.entities.movimientos.Movimiento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Clase que mapea los usuarios
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/21
 * @since 0.0.1 2020/10/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ToString(exclude = {"roles", "movimiento"})
@EqualsAndHashCode(exclude = {"roles", "movimiento"})
@Entity
@Table(name = "usuario")
public class Usuario
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   id;
    @Column(nullable = false, length = 250)
    private String nombreCompleto;
    
    @Column(nullable = false, length = 250, unique = true)
    @Email(message = "Ingresa un email valido")
    private String email;
    
    @Column(nullable = false, length = 250)
    @NotEmpty(message = "El campo password es obligatorio")
    private String password;
    
    @Column(nullable = false, name = "active")
    private boolean isActive;
    
    
    /** Roles asociados a un usuario */
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "usuario_rol", joinColumns = @JoinColumn(name = "usuario_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "rol_id", referencedColumnName = "id"))
    private Set<Rol> roles;
    
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    private Set<Movimiento> movimiento;
    
    @PrePersist
    public void init() {
      this.isActive = true;    
    }
}
