package com.kbm.kardex.infrastucture.entities.inventario;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kbm.kardex.infrastucture.entities.movimientos.DetalleFactura;
import com.kbm.kardex.infrastucture.entities.movimientos.Movimiento;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Clase que mapea los datos de los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ToString(exclude = { "unidad", "categoria", "detalleFactura"})
@EqualsAndHashCode(exclude = { "unidad", "categoria", "detalleFactura"})
@Entity
@Table(name = "producto")
public class Producto
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length = 250, nullable = false)
    private String nombre;
    
    @Column(length = 250, nullable = false)
    private String descripcion;
    /** Codigo o referencia */
    
    @Column(length = 10, nullable = false)
    private String codigo;
    
    @Column(nullable = false)
    private Double costo;
    
    @Column(length = 20, nullable = false)
    private String marca;
    
    @Column(length = 20, nullable = false)
    private String modelo;
    // TODO quitar cantidad
    @Column(nullable = true)
    private Integer cantidad;
    
    @Column(length = 300, nullable = true)
    private String observacion;
    
    @Column(length = 300, nullable = true)
    private String localizacion;
    
    @Column(length = 250, nullable = true)
    private String lote;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoria_id", insertable = false, updatable = false)
    private Categoria categoria;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "unidad_id", insertable = false, updatable = false)
    private Unidad unidad;
    
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "producto")
    private Movimiento movimiento;
    
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "producto", orphanRemoval = true)
    private Set<DetalleFactura> detalleFactura;
}
