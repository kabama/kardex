package com.kbm.kardex.infrastucture.entities.movimientos;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kbm.kardex.commons.enums.StatusModelo;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Clase que mapea las facturas
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ToString(exclude = {"proveedor", "detalleFactura"})
@EqualsAndHashCode(exclude = {"proveedor", "detalleFactura"})
@Entity
@Table(name = "factura")
public class Factura
{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @CreatedDate
    @NotNull(message = "El campo fechaCreacion es obligatorio")
    private LocalDate fechaCreacion;
    
    @Column(name = "numero_factura", length = 25, nullable = false, updatable = false)
    private String numeroFactura;
    
    @Column(nullable = false)
    private Long status;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "proveedor_id")
    private Usuario proveedor;
    
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "factura", orphanRemoval = true)
    private Set<DetalleFactura> detalleFactura;
    
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_movimiento_id")
    private TipoMovimiento tipoMovimiento;
    
    @PrePersist
    public void init() {
        this.status = StatusModelo.ACTIVO.getStatus();
    }
}
