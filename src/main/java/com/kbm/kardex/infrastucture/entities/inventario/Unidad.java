package com.kbm.kardex.infrastucture.entities.inventario;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Clase que mapea las unidades para los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/21
 * @since 0.0.1 2020/10/21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Entity
@Table(name = "unidad")
public class Unidad
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   id;
    @Column(nullable = false, length = 15)
    private String nombre;
    @Column(nullable = true, length = 100)
    private String descripcion;
}
