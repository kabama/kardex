package com.kbm.kardex.domain.service.inventario.impl;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.inventario.dto.UnidadDto;
import com.kbm.kardex.domain.service.inventario.UnidadService;
import com.kbm.kardex.infrastucture.entities.inventario.Unidad;
import com.kbm.kardex.infrastucture.repository.inventario.UnidadRepository;

import lombok.Getter;

/**
 * Servicio que implementa {@link UnidadService} para el manejo de las unidades
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Service
class UnidadImplService implements UnidadService
{
    
    @Autowired
    @Getter
    private UnidadRepository unidadRepository;
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.inventario.UnidadService#consultarUnidades()
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Override
    public List<UnidadDto> consultarUnidades()
    {
        List<Unidad> unidades = (List<Unidad>)getUnidadRepository().findAll();
        if(CollectionUtils.isEmpty(unidades)) {
            throw new DataNotFoundException("No se encontraon unidades");
        }
        return unidades.stream().filter(Objects::nonNull).map(mapearUnidad()).collect(Collectors.toList());
    }
    
    /**
     * Metdo que mapea un unidad
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @return Dto con datos mapeados
     */
    private Function<Unidad, UnidadDto> mapearUnidad()
    {
        return u ->
        {
            UnidadDto unidad = new UnidadDto();
            unidad.setId(u.getId());
            unidad.setNombre(u.getNombre());
            unidad.setDescripcion(u.getDescripcion());
            return unidad;
        };
    }
    
}
