package com.kbm.kardex.domain.service.administrativo.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kbm.kardex.infrastucture.entities.administrativo.Rol;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;

import lombok.Getter;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/25
 */
@Service
@Transactional
@Qualifier("kardex.usuarios")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    @Getter
    private UsuarioRepository userRepository;

    /* (non-Javadoc)
     * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
     */
    @Override
    public UserDetails loadUserByUsername(final String email) {
        Usuario usuario = getUserRepository().findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Usuario: 1%s no esta registrado en el sistema", email)));
        
        List<GrantedAuthority> authorities = usuario.getRoles().stream().map(Rol::getNombre).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        if (authorities.isEmpty())
        {
            throw new UsernameNotFoundException("Error en el Login: usuario '" + email + "' no tiene roles asignados!");
        }
 
        return new User(usuario.getEmail(), usuario.getPassword(), usuario.isActive(), true, true, true, authorities);
    }
}
