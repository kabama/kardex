package com.kbm.kardex.domain.service.inventario;

import java.util.List;

import com.kbm.kardex.domain.models.inventario.dto.UnidadDto;

/**
 * Interface para el manejo de las unidades de los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
public interface UnidadService
{
    /**
     * Metodo que consulta unidades
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @return listado de unidades
     */
    List<UnidadDto> consultarUnidades();
}
