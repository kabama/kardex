package com.kbm.kardex.domain.service.movimientos;

import java.util.List;

import com.kbm.kardex.domain.models.movimientos.dto.ConsultarMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearDetalleMovimientoDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.DetalleMovimientoKardexDto;
import com.kbm.kardex.domain.models.movimientos.dto.TipoMovimientoDto;

/**
 * Interface que maneja los movimientos de los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
public interface MovimientoService
{
    
    /**
     * Metodo para la creacion de un movimiento
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @param peticionCrear
     * @return
     */
    CrearMovimientoRespuestaDto crearMovimiento(CrearMovimientoPeticionDto peticionCrear);
    
    /**
     * Metodo que consulta los movimientos
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return
     */
    ConsultarMovimientoRespuestaDto buscarMovimientoPorId(Long id);
    
    // Integer cantidadProductoDisponiblesPorMovimiento(Long idMovimiento);
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/27
     * @since 0.0.1 2020/10/27
     * @param peticion
     * @return
     */
    DetalleMovimientoKardexDto crearDetalle(CrearDetalleMovimientoDto peticion);
    
    
    /**
     * Metodo que consulta los movimientos
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @return
     */
    ConsultarMovimientoRespuestaDto buscarMovimientoPorIdProducto(Long idProducto);
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/28
     * @since 0.0.1 2020/10/28
     * @return
     */
    List<TipoMovimientoDto> listaDeTipoMovimientos();
}
