package com.kbm.kardex.domain.service.movimientos.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import com.kbm.kardex.commons.enums.StatusModelo;
import com.kbm.kardex.domain.exceptions.BadRequestException;
import com.kbm.kardex.domain.exceptions.ConflictException;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearDetalleFacturaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaRespuestaDto;
import com.kbm.kardex.domain.service.movimientos.FacturaService;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;
import com.kbm.kardex.infrastucture.entities.movimientos.DetalleFactura;
import com.kbm.kardex.infrastucture.entities.movimientos.Factura;
import com.kbm.kardex.infrastucture.entities.movimientos.TipoMovimiento;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;
import com.kbm.kardex.infrastucture.repository.inventario.ProductoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.DetalleFacturaRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.FacturaRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.TipoMovimientoRepository;

import lombok.Getter;

/**
 * Servicio que implementa {@link FacturaService} para el manejo de las facturas
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/24
 */
@Service
@Validated
class FacturaImplService implements FacturaService
{
    
    @Autowired
    @Getter
    private FacturaRepository facturaRepository;
    
    @Autowired
    @Getter
    private DetalleFacturaRepository detalleFacturaRepository;
    
    @Autowired
    @Getter
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    @Getter
    private TipoMovimientoRepository tipoMovimientoRepository;
    
    @Autowired
    @Getter
    private ProductoRepository productoRepository;
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.FacturaService#findByProductoId(com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaPeticionDto)
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     */
    @Override
    public List<ConsultarDetalleFacturaRespuestaDto> findByProductoId(ConsultarDetalleFacturaPeticionDto peticionConsultar)
    {
        
        List<DetalleFactura> detalles = getDetalleFacturaRepository().findByProductoIdAndStatus(peticionConsultar.getIdProducto(), StatusModelo.ACTIVO.getStatus());
        if (CollectionUtils.isEmpty(detalles))
        {
            throw new DataNotFoundException(String.format("No existen detalles de facturas para el producto %s", peticionConsultar.getIdProducto()));
        }
        return detalles.stream().filter(Objects::nonNull).map(mapearConsultDetallefactura()).collect(Collectors.toList());
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.FacturaService#crearFactura(com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaPeticionDto)
     */
    @Override
    public CrearFacturaRespuestaDto crearFactura(CrearFacturaPeticionDto peticionCrear)
    {
        Usuario usuario = getUsuarioRepository().findById(peticionCrear.getIdProveedor()).orElseThrow(() -> new ConflictException("No existe el proveedor"));
        TipoMovimiento tipoMovimiento = getTipoMovimientoRepository().findById(peticionCrear.getIdTipoMovimiento()).orElseThrow(() -> new ConflictException("No existe el tipo de movimiento"));
        Set<DetalleFactura> detalles = new HashSet<>();
        
        Factura factura = Factura.builder().fechaCreacion(peticionCrear.getFechaCreacion()).numeroFactura(peticionCrear.getNumeroFactura()).proveedor(usuario).tipoMovimiento(tipoMovimiento).build();
        
        for (CrearDetalleFacturaDto detallesMapear : peticionCrear.getDetalles())
        {
            DetalleFactura detalle = this.mapearCrearDetalle().apply(detallesMapear, factura);
            detalles.add(detalle);
        }
        factura.setDetalleFactura(detalles);
        factura = getFacturaRepository().save(factura);
        return mapearFactura().apply(factura);
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.FacturaService#eliminarFactura(java.lang.Long)
     */
    @Override
    public void eliminarFactura(Long id)
    {
        try
        {
            getFacturaRepository().deleteById(id);
        }
        catch (Exception e)
        {
            throw new BadRequestException("No se pudo eliminar la factura");
        }
        
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.FacturaService#listaFacturas()
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     */
    @Override
    public List<CrearFacturaRespuestaDto> listaFacturas()
    {
        List<Factura> facturas = getFacturaRepository().findAll();
        if (CollectionUtils.isEmpty(facturas))
        {
            throw new DataNotFoundException("No se encontraron facturas");
        }
        
        return facturas.stream().filter(Objects::nonNull).map(mapearFactura()).collect(Collectors.toList());
    }
    
    /**
     * Metodo que mapea el detalle de factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private Function<DetalleFactura, ConsultarDetalleFacturaRespuestaDto> mapearConsultDetallefactura()
    {
        return detalle ->
        {
            ConsultarDetalleFacturaRespuestaDto respuesta = new ConsultarDetalleFacturaRespuestaDto();
            respuesta.setCantidad(detalle.getCantidad());
            respuesta.setDescripcion(detalle.getDescripcion());
            respuesta.setDetalleKardex(String.format("%s %s %s. a %s C/U Fact No %s", detalle.getFactura().getTipoMovimiento().getNombre(), detalle.getCantidad(),
                    detalle.getProducto().getUnidad().getNombre(), detalle.getValorUnitario(), detalle.getFactura().getNumeroFactura()));
            respuesta.setFechaDetalleKardex(detalle.getFecha().toString());
            respuesta.setFechaMovimiento(detalle.getFecha());
            respuesta.setNumeroFactura(detalle.getFactura().getNumeroFactura());
            respuesta.setTipoMovimientoId(detalle.getFactura().getTipoMovimiento().getId());
            respuesta.setValorUnitario(detalle.getValorUnitario());
            respuesta.setIdFactura(detalle.getFactura().getId());
            respuesta.setIdDetalleFactura(detalle.getId());
            return respuesta;
        };
    }
    
    /**
     * Metodo que mapea un detalle de factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private Function<DetalleFactura, CrearDetalleFacturaDto> mapearDetalle()
    {
        return detalleFactura ->
        {
            CrearDetalleFacturaDto detalle = new CrearDetalleFacturaDto();
            detalle.setCantidad(detalleFactura.getCantidad());
            detalle.setDescripcion(detalleFactura.getDescripcion());
            detalle.setIdProducto(detalleFactura.getProducto().getId());
            detalle.setValorTotal(detalleFactura.getValorTotal());
            detalle.setValorUnitario(detalleFactura.getValorUnitario());
            return detalle;
        };
    }
    /**
     * Metodo que mapea un detalle de factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private BiFunction<CrearDetalleFacturaDto, Factura, DetalleFactura> mapearCrearDetalle()
    {
        return (detalleFactura, factura) ->
        {
            DetalleFactura detalle = new DetalleFactura();
            detalle.setCantidad(detalleFactura.getCantidad());
            detalle.setDescripcion(detalleFactura.getDescripcion());
            Producto producto = getProductoRepository().findById(detalleFactura.getIdProducto())
                    .orElseThrow(() -> new DataNotFoundException("El producto a agregar en el detalle de la factura no existe"));
            detalle.setProducto(producto);
            detalle.setValorTotal(detalleFactura.getValorTotal());
            detalle.setValorUnitario(detalleFactura.getValorUnitario());
            detalle.setFactura(factura);
            return detalle;
        };
    }
    
    /**
     * Metodo que mapea los datos de una factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private Function<Factura, CrearFacturaRespuestaDto> mapearFactura()
    {
        return factura ->
        {
            
            CrearFacturaRespuestaDto respuesta = new CrearFacturaRespuestaDto();
            respuesta.setFechaCreacion(factura.getFechaCreacion());
            respuesta.setId(factura.getId());
            respuesta.setIdProveedor(factura.getProveedor().getId());
            respuesta.setIdTipoMovimiento(factura.getTipoMovimiento().getId());
            respuesta.setNombreMovimiento(factura.getTipoMovimiento().getNombre());
            respuesta.setNombreProveedor(factura.getProveedor().getNombreCompleto());
            respuesta.setNumeroFactura(factura.getNumeroFactura());
            List<CrearDetalleFacturaDto> detalles = factura.getDetalleFactura().stream().filter(Objects::nonNull).map(mapearDetalle()).collect(Collectors.toList());
            respuesta.setDetalles(detalles);
            return respuesta;
        };
    }
    
}
