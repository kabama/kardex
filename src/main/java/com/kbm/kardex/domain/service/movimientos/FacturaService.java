package com.kbm.kardex.domain.service.movimientos;

import java.util.List;

import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarDetalleFacturaRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearFacturaRespuestaDto;

/**
 * Interface para el manejo de las facturas
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
public interface FacturaService
{
    
    
    /**
     * Busca los detalles de facturas por id de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param id
     * @return
     */
    List<ConsultarDetalleFacturaRespuestaDto> findByProductoId(ConsultarDetalleFacturaPeticionDto peticionConsultar);
    
    /**
     * Metodo encargado de crear una factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param peticionCrear
     * @return
     */
    CrearFacturaRespuestaDto crearFactura(CrearFacturaPeticionDto peticionCrear);
    
    
    /**
     * Metodo que elimina una factura
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param id
     */
    void eliminarFactura(Long id);
    
    /**
     * Busca los detalles de facturas por id de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @param id
     * @return
     */
    List<CrearFacturaRespuestaDto> listaFacturas();
}
