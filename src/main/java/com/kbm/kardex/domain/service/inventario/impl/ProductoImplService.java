package com.kbm.kardex.domain.service.inventario.impl;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.inventario.dto.ProductoDto;
import com.kbm.kardex.domain.service.inventario.ProductoService;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;
import com.kbm.kardex.infrastucture.repository.inventario.ProductoRepository;

import lombok.Getter;

/**
 * Servicio que implementa {@link ProductoService} para el manejo de los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Service
class ProductoImplService implements ProductoService
{
    /** Repositorio para el manejo de productos */
    @Autowired
    @Getter
    private ProductoRepository productoRepository;
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.inventario.ProductoService#findByMarca(java.lang.String)
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Override
    public List<ProductoDto> findByMarca(String marca)
    {
        List<Producto> productos = getProductoRepository().findByMarca(marca);
        if (CollectionUtils.isEmpty(productos))
        {
            throw new DataNotFoundException(String.format("No se encontraron productos de la marca %s", marca));
        }
        return productos.stream().filter(Objects::nonNull).map(mapearProducto()).collect(Collectors.toList());
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.inventario.ProductoService#findByNombre(java.lang.String)
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Override
    public List<ProductoDto> findByNombre(String nombre)
    {
        List<Producto> productos = getProductoRepository().findByNombre(nombre);
        if (CollectionUtils.isEmpty(productos))
        {
            throw new DataNotFoundException(String.format("No se encontraron productos por el nombre %s", nombre));
        }
        return productos.stream().filter(Objects::nonNull).map(mapearProducto()).collect(Collectors.toList());
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.inventario.ProductoService#findByCodigo(java.lang.String)
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Override
    public List<ProductoDto> findByCodigo(String codigo)
    {
        List<Producto> productos = getProductoRepository().findByCodigo(codigo);
        if (CollectionUtils.isEmpty(productos))
        {
            throw new DataNotFoundException(String.format("No se encontraron productos por el codigo %s", codigo));
        }
        return productos.stream().filter(Objects::nonNull).map(mapearProducto()).collect(Collectors.toList());
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.inventario.ProductoService#findAll()
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Override
    public List<ProductoDto> findAll()
    {
        List<Producto> productos = getProductoRepository().findAll();
        if (CollectionUtils.isEmpty(productos))
        {
            throw new DataNotFoundException("No se encontraron productos");
        }
        return productos.stream().filter(Objects::nonNull).map(mapearProducto()).collect(Collectors.toList());
    }
    
    /**
     * Metodo que mapea los datos de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @return dto con los datos mapeados
     */
    private Function<Producto, ProductoDto> mapearProducto()
    {
        return product ->
        {
            ProductoDto dto = new ProductoDto();
            dto.setId(product.getId());
            dto.setNombre(product.getNombre());
            dto.setCantidad(product.getCantidad());
            dto.setCodigo(product.getCodigo());
            dto.setMarca(product.getMarca());
            dto.setDescripcion(product.getDescripcion());
            dto.setModelo(product.getModelo());
            dto.setObservacion(product.getObservacion());
            return dto;
        };
    }

    /* (non-Javadoc)
     * @see com.kbm.kardex.domain.service.inventario.ProductoService#findById(java.lang.Long)
     */
    @Override
    public ProductoDto findById(Long id)
    {
        Producto producto = getProductoRepository().findById(id).orElseThrow(() -> new DataNotFoundException("No se encontraron productos"));
        return mapearProducto().apply(producto);
    }
    
}
