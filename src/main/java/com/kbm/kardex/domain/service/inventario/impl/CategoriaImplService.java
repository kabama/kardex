package com.kbm.kardex.domain.service.inventario.impl;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.inventario.dto.CategoriaDto;
import com.kbm.kardex.domain.service.inventario.CategoriaService;
import com.kbm.kardex.infrastucture.entities.inventario.Categoria;
import com.kbm.kardex.infrastucture.repository.inventario.CategoriaRepository;

import lombok.Getter;

/**
 * Servicio que implementa {@link CategoriaService} para el manejo de las categorias
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Service
class CategoriaImplService implements CategoriaService
{
    
    @Autowired
    @Getter
    private CategoriaRepository categoriaRepository;
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.inventario.CategoriaService#consultarCategorias()
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     */
    @Override
    public List<CategoriaDto> consultarCategorias()
    {
        List<Categoria> categorias = ((List<Categoria>) categoriaRepository.findAll());
        if (CollectionUtils.isEmpty(categorias))
        {
            throw new DataNotFoundException("No se encontraron categorias");
        }
        return categorias.stream().map(mapearCategoria()).filter(Objects::nonNull).collect(Collectors.toList());
    }
    
    /**
     * Metodo que mapea las categorias
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param categoria
     * @return
     */
    private Function<Categoria, CategoriaDto> mapearCategoria()
    {
        return e ->
        {
            CategoriaDto dto = new CategoriaDto();
            dto.setId(e.getId());
            dto.setNombre(e.getNombre());
            dto.setDescripcion(e.getDescripcion());
            return dto;
        };
    }
    
}
