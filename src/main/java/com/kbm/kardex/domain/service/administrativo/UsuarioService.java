package com.kbm.kardex.domain.service.administrativo;

import java.util.List;

import com.kbm.kardex.domain.models.administrativo.dto.ProveedorDto;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreatePeticionDto;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreateRespuestaDto;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/26
 * @since 0.0.1 2020/10/26
 */
public interface UsuarioService
{
 
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/25
     * @since 0.0.1 2020/10/25
     * @param registrar
     * @return
     */
    UsuarioCreateRespuestaDto createUsuario(UsuarioCreatePeticionDto registrar);
    
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/25
     * @since 0.0.1 2020/10/25
     * @param registrar
     * @return
     */
    UsuarioCreateRespuestaDto cargarDatosUsuarioPorEmail(String email);
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/25
     * @since 0.0.1 2020/10/25
     * @param registrar
     * @return
     */
    List<ProveedorDto> cargarProveedores(Long idRol);
    
}
