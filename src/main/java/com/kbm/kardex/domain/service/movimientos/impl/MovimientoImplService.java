package com.kbm.kardex.domain.service.movimientos.impl;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import com.kbm.kardex.commons.enums.StatusModelo;
import com.kbm.kardex.domain.exceptions.ConflictException;
import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.movimientos.dto.ConsultarMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearDetalleMovimientoDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoPeticionDto;
import com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoRespuestaDto;
import com.kbm.kardex.domain.models.movimientos.dto.DetalleMovimientoKardexDto;
import com.kbm.kardex.domain.models.movimientos.dto.TipoMovimientoDto;
import com.kbm.kardex.domain.models.movimientos.dto.enumerador.TipoMovimientoEnum;
import com.kbm.kardex.domain.service.movimientos.MovimientoService;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.entities.inventario.Producto;
import com.kbm.kardex.infrastucture.entities.movimientos.DetalleMovimiento;
import com.kbm.kardex.infrastucture.entities.movimientos.Movimiento;
import com.kbm.kardex.infrastucture.entities.movimientos.TipoMovimiento;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;
import com.kbm.kardex.infrastucture.repository.inventario.ProductoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.DetalleFacturaRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.DetalleMovimientoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.MovimientoRepository;
import com.kbm.kardex.infrastucture.repository.movimientos.TipoMovimientoRepository;

import lombok.Getter;

/**
 * Servicio que implementa {@link MovimientoService} para el manejo de los movimientos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@Service
@Validated
class MovimientoImplService implements MovimientoService
{
    
    private static final int CANTIDAD_MINIMA_PRODCUTOS = 0;
    
    @Autowired
    @Getter
    private MovimientoRepository movimientoRepository;
    
    @Autowired
    @Getter
    private DetalleMovimientoRepository detalleMovimientoRepository;
    
    @Autowired
    @Getter
    private TipoMovimientoRepository tipoMovimientoRepository;
    
    @Autowired
    @Getter
    private ProductoRepository productoRepository;
    
    @Autowired
    @Getter
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    @Getter
    private DetalleFacturaRepository detallefacturaRepository;
    
    /**
     * Metodo que calcula el costo unitario
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @param cantidad
     * @param valorUnitario
     * @return costo unitario
     */
    BiFunction<Integer, Double, Double> calcularValorEntradaSalida = (cantidad, valorUnitario) -> cantidad * valorUnitario;
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.MovimientoService#crearDetalle(com.kbm.kardex.domain.models.movimientos.dto.CrearDetalleMovimientoDto)
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     */
    @Override
    public DetalleMovimientoKardexDto crearDetalle(CrearDetalleMovimientoDto peticion)
    {
        DetalleMovimientoKardexDto respuesta = null;
        
        if (Objects.isNull(peticion))
        {
            throw new ConflictException("El detalle no puede ser nulo");
        }
        
        Double valor = calcularValorEntradaSalida.apply(peticion.getCantidad(), peticion.getValorUnitario());
        DetalleMovimiento ultimoDetalle = detalleMovimientoRepository.findTopByMovimientoIdOrderByIdDesc(peticion.getIdMovimiento());
        if (Objects.isNull(ultimoDetalle))
        {
            peticion.setTipoMovimientoId(TipoMovimientoEnum.INVENTARIO_INICIAL.getTipo());
            peticion.setCantidadSaldo(peticion.getCantidad());
            peticion.setValor(valor);
            peticion.setValorSaldo(valor);
            respuesta = ejecutarCrearDetalleInicial(peticion);
        }
        else
        { 
           
            if (Objects.nonNull(ultimoDetalle.getCantidadSaldo()) && ultimoDetalle.getCantidadSaldo().equals(CANTIDAD_MINIMA_PRODCUTOS))
            {
                throw new ConflictException("No tiene mas productos disponibles para realizar movimientos");
            }
            
            if (peticion.getTipoMovimientoId().equals(TipoMovimientoEnum.COMPRA.getTipo()) || peticion.getTipoMovimientoId().equals(TipoMovimientoEnum.DEVOLUCION_VENTA.getTipo()))
            {
                Integer cantidadSaldo = ultimoDetalle.getCantidadSaldo() + peticion.getCantidad();
                Double valorSaldo = valor + ultimoDetalle.getValorSaldo();
                peticion.setCantidadSaldo(cantidadSaldo);
                peticion.setValor(valor);
                peticion.setValorSaldo(valorSaldo);
                respuesta = ejecutarCrearDetalleInicial(peticion);
            }
            else if (peticion.getTipoMovimientoId().equals(TipoMovimientoEnum.VENTA.getTipo()) || peticion.getTipoMovimientoId().equals(TipoMovimientoEnum.DEVOLUCION_COMPRA.getTipo()))
            {
                Integer cantidadSaldo = ultimoDetalle.getCantidadSaldo() - peticion.getCantidad();
                Double valorSaldo = ultimoDetalle.getValorSaldo() - valor;
                peticion.setCantidadSaldo(cantidadSaldo);
                peticion.setValor(valor);
                peticion.setValorSaldo(valorSaldo);
                
                respuesta = ejecutarCrearDetalleInicial(peticion);
            }
        }
        
        return respuesta;
    }
    
    /**
     * Metodo que ejecuta el crear detalle en movimiento
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @param peticion
     * @return
     */
    private DetalleMovimientoKardexDto ejecutarCrearDetalleInicial(CrearDetalleMovimientoDto peticion)
    {
        DetalleMovimiento detalle = datosInicialesCrearDetalle(peticion);
        return mapearDetalle().apply(detalle);
    }

 
    
    /**
     * Metodo que actualiza un movimiento
     * @author aleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     * @param peticios dto con datos de la peticion
     */
    private DetalleMovimiento datosInicialesCrearDetalle(CrearDetalleMovimientoDto peticion)
    {
        Movimiento movimiento = getMovimientoRepository().findById(peticion.getIdMovimiento())
                .orElseThrow(() -> new ConflictException(String.format("No existe el movimiento [%s]", peticion.getIdMovimiento())));
        TipoMovimiento tipoMovimiento = getTipoMovimientoRepository().findById(peticion.getTipoMovimientoId())
                .orElseThrow(() -> new ConflictException(String.format("No existe el tipo de movimiento: [%s]", peticion.getTipoMovimientoId())));
        
        DetalleMovimiento detalle = DetalleMovimiento.builder().cantidad(peticion.getCantidad()).valorUnitario(peticion.getValorUnitario()).cantidadSaldo(peticion.getCantidadSaldo())
                .valor(peticion.getValor()).valorSaldo(peticion.getValorSaldo()).descripcion(peticion.getDescripcion()).fechaMovimiento(peticion.getFechaMovimiento()).movimiento(movimiento)
                .tipoMovimiento(tipoMovimiento).build();
        
        if (CollectionUtils.isEmpty(movimiento.getDetalleMovimiento()))
        {
            final Set<DetalleMovimiento> detalles = new HashSet<>();
            detalles.add(detalle);
            movimiento.getDetalleMovimiento().clear();
            movimiento.getDetalleMovimiento().addAll(detalles);
        }
        else
        {
            movimiento.getDetalleMovimiento().add(detalle);
        }
        getMovimientoRepository().save(movimiento);
        getDetallefacturaRepository().actualizarEstado(StatusModelo.INACTIVO.getStatus(), peticion.getIdDetalleFactura());
        return detalle;
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.MovimientoService#crearMovimiento(com.kbm.kardex.domain.models.movimientos.dto.CrearMovimientoPeticionDto)
     * @author aleth Bahena
     * @version 0.0.1 2020/10/23
     * @since 0.0.1 2020/10/23
     */
    @Override
    public CrearMovimientoRespuestaDto crearMovimiento(CrearMovimientoPeticionDto peticionCrear)
    {
        Producto producto = getProductoRepository().findById(peticionCrear.getProductoId()).orElseThrow(() -> new ConflictException("El producto que intentas asociar al kardex no es valido"));
        Usuario usuario = getUsuarioRepository().findById(peticionCrear.getUsuarioId()).orElseThrow(() -> new ConflictException("El usuario que intentas asociar al kardex no es valido"));
        Movimiento movimiento = Movimiento.builder().maximo(peticionCrear.getMaximo()).minimo(peticionCrear.getMinimo()).observaciones(peticionCrear.getObservaciones()).producto(producto)
                .usuario(usuario).build();
        getMovimientoRepository().save(movimiento);
        CrearMovimientoRespuestaDto respuesta = new CrearMovimientoRespuestaDto();
        respuesta.setId(movimiento.getId());
        respuesta.setMaximo(movimiento.getMaximo());
        respuesta.setMinimo(movimiento.getMinimo());
        respuesta.setProductoLocalizacion(movimiento.getProducto().getLocalizacion());
        respuesta.setProductoNombre(movimiento.getProducto().getNombre());
        respuesta.setProductoReferencia(movimiento.getProducto().getCodigo());
        respuesta.setProductoUnidades(movimiento.getProducto().getUnidad().getNombre());
        respuesta.setProveedorNombre(movimiento.getUsuario().getNombreCompleto());
        return respuesta;
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.MovimientoService#buscarMovimientoPorId(java.lang.Long)
     */
    @Override
    public ConsultarMovimientoRespuestaDto buscarMovimientoPorId(Long id)
    {
        Movimiento movimiento = getMovimientoRepository().findById(id).orElseThrow(() -> new DataNotFoundException(String.format("El movimiento con id % no existe", id)));
        ConsultarMovimientoRespuestaDto respuesta = new ConsultarMovimientoRespuestaDto();
        List<DetalleMovimientoKardexDto> detales = movimiento.getDetalleMovimiento().stream().filter(Objects::nonNull).map(mapearDetalle()).collect(Collectors.toList());
        respuesta.setId(movimiento.getId());
        respuesta.setMaximo(movimiento.getMaximo());
        respuesta.setMinimo(movimiento.getMinimo());
        respuesta.setProductoLocalizacion(movimiento.getProducto().getLocalizacion());
        respuesta.setProductoNombre(movimiento.getProducto().getNombre());
        respuesta.setProductoReferencia(movimiento.getProducto().getCodigo());
        respuesta.setProductoUnidades(movimiento.getProducto().getUnidad().getNombre());
        respuesta.setDetalles(detales);
        return respuesta;
    }
    
    /**
     * Metodo que mapea un detalle de un movimiento
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/24
     * @since 0.0.1 2020/10/24
     * @return
     */
    private Function<DetalleMovimiento, DetalleMovimientoKardexDto> mapearDetalle()
    {
        return e ->
        {
            DetalleMovimientoKardexDto detalle = new DetalleMovimientoKardexDto();
            if (e.getTipoMovimiento().getId().equals(TipoMovimientoEnum.COMPRA.getTipo()) || e.getTipoMovimiento().getId().equals(TipoMovimientoEnum.DEVOLUCION_COMPRA.getTipo()) || e.getTipoMovimiento().getId().equals(TipoMovimientoEnum.INVENTARIO_INICIAL.getTipo()))
            {
                detalle.setCantidadEntrada(e.getCantidad());
                detalle.setValorEntrada(e.getValor());
                detalle.setValorUnitarioEntrada(e.getValorUnitario());
                detalle.setCantidadSalida(0);
                detalle.setValorSalida(0.0);
                detalle.setValorUnitarioSalida(0.0);
            }
            else
            {
                detalle.setCantidadSalida(e.getCantidad());
                detalle.setValorSalida(e.getValor());
                detalle.setValorUnitarioSalida(e.getValorUnitario());
                detalle.setCantidadEntrada(0);
                detalle.setValorEntrada(0.0);
                detalle.setValorUnitarioEntrada(0.0);
            }
            detalle.setId(e.getId());
            detalle.setDescripcion(e.getDescripcion());
            detalle.setFechaMovimiento(e.getFechaMovimiento());
            detalle.setIdMovimiento(e.getMovimiento().getId());
            detalle.setValorSaldo(e.getValorSaldo());
            detalle.setCantidadSaldo(e.getCantidadSaldo());
            return detalle;
        };
    }
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.MovimientoService#buscarMovimientoPorIdProducto(java.lang.Long)
     */
    @Override
    public ConsultarMovimientoRespuestaDto buscarMovimientoPorIdProducto(Long idProducto)
    {
        
        Movimiento movimiento = getMovimientoRepository().findByProductoIdOrderByIdAsc(idProducto).orElseThrow(() -> new DataNotFoundException("El producto no tiene movimiento asociado"));
        ConsultarMovimientoRespuestaDto movimientoDto = new ConsultarMovimientoRespuestaDto();
        movimientoDto.setId(movimiento.getId());
        movimientoDto.setMaximo(movimiento.getMaximo());
        movimientoDto.setMinimo(movimiento.getMinimo());
        movimientoDto.setProductoLocalizacion(movimiento.getProducto().getLocalizacion());
        movimientoDto.setProductoNombre(movimiento.getProducto().getNombre());
        movimientoDto.setProductoReferencia(movimiento.getProducto().getCodigo());
        movimientoDto.setProductoUnidades(movimiento.getProducto().getUnidad().getNombre());
        movimientoDto.setProveedorNombre(movimiento.getUsuario().getNombreCompleto());
        movimientoDto.setIdProducto(idProducto);
        movimientoDto.setIdMovimiento(movimiento.getId());
        List<DetalleMovimientoKardexDto> detalles = movimiento.getDetalleMovimiento().stream().filter(Objects::nonNull).map(mapearDetalle()).sorted(Comparator.comparing(DetalleMovimientoKardexDto::getId)).collect(Collectors.toList());
        movimientoDto.setDetalles(detalles);
        return movimientoDto;
    }

    /* (non-Javadoc)
     * @see com.kbm.kardex.domain.service.movimientos.MovimientoService#listaDeTipoMovimientos()
     */
    @Override
    public List<TipoMovimientoDto> listaDeTipoMovimientos()
    {
        List<TipoMovimiento> listaTipoMovimiento = (List<TipoMovimiento>)getTipoMovimientoRepository().findAll();
        if(CollectionUtils.isEmpty(listaTipoMovimiento)) {
            throw new DataNotFoundException("No se encontraron tipo de movimientos");
        }
        return listaTipoMovimiento.stream().filter(Objects::nonNull).filter(e -> !e.getId().equals(TipoMovimientoEnum.INVENTARIO_INICIAL.getTipo())).map(mapearTipoMovimiento()).collect(Collectors.toList());
    }
    
    /**
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/28
     * @since 0.0.1 2020/10/28
     * @return
     */
    private Function<TipoMovimiento, TipoMovimientoDto> mapearTipoMovimiento(){
        return tipo -> {
            TipoMovimientoDto dto = new TipoMovimientoDto();
            dto.setId(tipo.getId());
            dto.setNombre(tipo.getNombre());
          return dto;  
        };
    }
}
