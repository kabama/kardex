package com.kbm.kardex.domain.service.inventario;

import java.util.List;

import com.kbm.kardex.domain.models.inventario.dto.CategoriaDto;

/**
 * Interface para el manejo de las categorias de los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
public interface CategoriaService
{
    
    /**
     * Metodo que conulta las categorias
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @return Listado de categorias
     */
    List<CategoriaDto> consultarCategorias();
}
