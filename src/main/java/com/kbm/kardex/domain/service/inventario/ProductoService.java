package com.kbm.kardex.domain.service.inventario;

import java.util.List;

import com.kbm.kardex.domain.models.inventario.dto.ProductoDto;

/**
 * Interface para el manejo de los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
public interface ProductoService
{
    
    /**
     * Consulta todos los productos
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param marca
     * @return
     */
    List<ProductoDto> findAll();
    
    
    /**
     * Consulta por marca 
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param marca
     * @return
     */
    List<ProductoDto> findByMarca(String marca);
    
    
    /**
     * Consulta por nombre
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param nombre
     * @return
     */
    List<ProductoDto> findByNombre(String nombre);
    
    
    /**
     * Consulta por codigo de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param codigo
     * @return
     */
    List<ProductoDto> findByCodigo(String codigo);
    
    
    /**
     * Consulta por codigo de producto
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/22
     * @since 0.0.1 2020/10/22
     * @param codigo
     * @return
     */
    ProductoDto findById(Long id);
}
