package com.kbm.kardex.domain.service.administrativo.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import com.kbm.kardex.domain.exceptions.DataNotFoundException;
import com.kbm.kardex.domain.models.administrativo.dto.ProveedorDto;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreatePeticionDto;
import com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreateRespuestaDto;
import com.kbm.kardex.domain.service.administrativo.UsuarioService;
import com.kbm.kardex.infrastucture.entities.administrativo.Rol;
import com.kbm.kardex.infrastucture.entities.administrativo.Usuario;
import com.kbm.kardex.infrastucture.repository.administrativo.RolRepository;
import com.kbm.kardex.infrastucture.repository.administrativo.UsuarioRepository;

import lombok.Getter;

/**
 * Servicio que implementa {@link UsuarioService} para el manejo de los usuarios
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/25
 */
@Service
@Validated
class UsuarioImplService implements UsuarioService
{
    
    @Autowired
    @Getter
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    @Getter
    private RolRepository rolRepository;
    
    @Autowired
    private PasswordEncoder bPasswordEnconder;
    
    /*
     * (non-Javadoc)
     * @see com.kbm.kardex.domain.service.administrativo.UsuarioService#createUsuario(com.kbm.kardex.domain.models.administrativo.dto.UsuarioCreateDto)
     */
    @Override
    public UsuarioCreateRespuestaDto createUsuario(UsuarioCreatePeticionDto registrar)
    {
        Rol rol = getRolRepository().findById(registrar.getIdRol()).orElseThrow(() -> new DataNotFoundException("El rol que intentas asociar al usuario no existe"));
        final Set<Rol> roles = new HashSet<>();
        roles.add(rol);
        Usuario usuario = Usuario.builder().email(registrar.getEmail()).roles(roles).nombreCompleto(registrar.getNombreCompleto()).password(bPasswordEnconder.encode(registrar.getPassword())).build();
        usuario = getUsuarioRepository().save(usuario);
        UsuarioCreateRespuestaDto respuestaCrearUsuario = new UsuarioCreateRespuestaDto();
        respuestaCrearUsuario.setEmail(usuario.getEmail());
        respuestaCrearUsuario.setNombreCompleto(usuario.getNombreCompleto());
        return respuestaCrearUsuario;
    }

    /* (non-Javadoc)
     * @see com.kbm.kardex.domain.service.administrativo.UsuarioService#cargarDatosUsuariPorEmail(java.lang.String)
     */
    @Override
    public UsuarioCreateRespuestaDto cargarDatosUsuarioPorEmail(String email)
    {
        Usuario usuario = getUsuarioRepository().findByEmail(email).orElseThrow(() -> new DataNotFoundException("El usuario no existe"));
        UsuarioCreateRespuestaDto respuestaCrearUsuario = new UsuarioCreateRespuestaDto();
        respuestaCrearUsuario.setEmail(usuario.getEmail());
        respuestaCrearUsuario.setNombreCompleto(usuario.getNombreCompleto());
        return respuestaCrearUsuario;
    }

    /* (non-Javadoc)
     * @see com.kbm.kardex.domain.service.administrativo.UsuarioService#cargarProveedores(java.lang.String)
     */
    @Override
    public List<ProveedorDto> cargarProveedores(Long  rolId)
    {
        List<Usuario> proveedores = getUsuarioRepository().findByRolesId(rolId);
        if(CollectionUtils.isEmpty(proveedores)) {
            throw new DataNotFoundException("No hay proveedores registrados");
        }
        return proveedores.stream().filter(Objects::nonNull).map(mapearUsuarioCreateRespuestaDtoo()).collect(Collectors.toList());
    }
    
    /**
     * Metodo para el mapeo de usuario
     * @author Kaleth Bahena
     * @version 0.0.1 2020/10/26
     * @since 0.0.1 2020/10/26
     * @return
     */
    private Function<Usuario, ProveedorDto> mapearUsuarioCreateRespuestaDtoo(){
        return usuario -> {
            ProveedorDto dto = new ProveedorDto();
            dto.setEmail(usuario.getEmail());
            dto.setNombreCompleto(usuario.getNombreCompleto());
            dto.setId(usuario.getId());
            return dto;
        };
    }
}
