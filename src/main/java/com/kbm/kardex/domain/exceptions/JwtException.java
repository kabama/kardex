package com.kbm.kardex.domain.exceptions;

public class JwtException extends UnauthorizedException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public JwtException(String detail) {
        super(detail);
    }

}