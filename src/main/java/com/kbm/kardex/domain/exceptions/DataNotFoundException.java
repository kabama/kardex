package com.kbm.kardex.domain.exceptions;

/**
 * Clase que mapea el tipo error 204
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
public class DataNotFoundException extends RuntimeException {
    /** Default serial*/
    private static final long serialVersionUID = 1L;

    public DataNotFoundException(String detail) {
        super(detail);
    }

}
