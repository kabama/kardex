package com.kbm.kardex.domain.exceptions;

/**
 * Clase que mapea error 400
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
public class BadRequestException extends RuntimeException
{
    /** Default serial*/
    private static final long serialVersionUID = 1L;
    
    public BadRequestException(String detail)
    {
        super(detail);
    }
    
}
