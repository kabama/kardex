package com.kbm.kardex.domain.exceptions;

/**
 * Clase para el mapeo error usuario no autorizados
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/25
 */
public class UnauthorizedException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public UnauthorizedException(String detail) {
        super(detail);
    }

}