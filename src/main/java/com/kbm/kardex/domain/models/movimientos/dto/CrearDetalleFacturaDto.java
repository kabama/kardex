package com.kbm.kardex.domain.models.movimientos.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * Clase que mapea los datos para la creacion de detalle de factura
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Data
public class CrearDetalleFacturaDto
{
    @Min(message = "La cantidad debe ser minimo un producto", value = 1)
    private Integer cantidad;
    @NotBlank(message = "La descripcion es obligatoria")
    private String descripcion;
    @NotBlank(message = "El valor unitario es obligatoria")
    private Double valorTotal;
    @NotBlank(message = "El valor unitario es obligatoria")
    private Double valorUnitario;
    @NotBlank(message = "El identificador del producto es obligatoria")
    private Long idProducto;
}
