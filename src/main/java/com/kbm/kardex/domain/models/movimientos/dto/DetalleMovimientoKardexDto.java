package com.kbm.kardex.domain.models.movimientos.dto;

import java.time.LocalDate;

import lombok.Data;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/27
 * @since 0.0.1 2020/10/27
 */
@Data
public class DetalleMovimientoKardexDto
{
    private Long      id;
    private Long      idMovimiento;
    private LocalDate fechaMovimiento;
    private String    descripcion;
    private Double    valorUnitarioEntrada;
    private Integer   cantidadEntrada;
    private Double    valorEntrada;
    private Double    valorUnitarioSalida;
    private Integer   cantidadSalida;
    private Double    valorSalida;
    private Integer   cantidadSaldo;
    private Double    valorSaldo;
}
