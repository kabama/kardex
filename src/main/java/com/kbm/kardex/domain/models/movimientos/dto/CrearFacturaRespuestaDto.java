package com.kbm.kardex.domain.models.movimientos.dto;

import java.time.LocalDate;
import java.util.List;

import lombok.Data;

/**
 * Clase que mapea los datos para la creacion de una factura
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Data
public class CrearFacturaRespuestaDto
{
    private Long id;
    private LocalDate fechaCreacion;
    private String numeroFactura;
    private String nombreProveedor;
    private Long idProveedor;
    private Long idTipoMovimiento;
    private String nombreMovimiento;
    
    private List<CrearDetalleFacturaDto> detalles;

}
