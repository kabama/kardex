package com.kbm.kardex.domain.models.administrativo.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Dto para el mapeo del registro de usuario
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/25
 */
@Data
public class UsuarioCreatePeticionDto
{
    @NotBlank(message = "El campo nombreCompleto es obligatorio")
    private String nombreCompleto;
    
    @Email(message = "Ingresa un email valido")
    @NotBlank(message = "El campo email es obligatorio")
    private String email;
    
    @NotBlank(message = "El campo password es obligatorio")
    @Min(message = "El password debe tener minimo 8 digitos", value = 8)

    private String password;
    
    @NotNull(message = "El campo rol es obligatorio")
    private Long idRol;
}
