package com.kbm.kardex.domain.models.inventario.dto;

import lombok.Data;

/**
 * Clase que mapea las categorias
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Data
public class CategoriaDto
{
    private Long id;
    private String nombre;
    private String descripcion;
}
