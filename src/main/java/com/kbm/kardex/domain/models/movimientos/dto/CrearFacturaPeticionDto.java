package com.kbm.kardex.domain.models.movimientos.dto;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Clase que mape los datos para crear una factura
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Data
public class CrearFacturaPeticionDto
{
    @NotNull(message = "El campo fecha de creacion es obligatorio")
    private LocalDate fechaCreacion;
    @NotNull(message = "El campo proveedor es obligatorio")
    private Long idProveedor;
    @NotNull(message = "El campo tipo movimiento es obligatorio")
    private Long idTipoMovimiento;
    @NotNull(message = "El campo numero factura es obligatorio")
    private String numeroFactura;
    
    @NotEmpty(message = "Debe agregar minimo un detealle para la factura")
    private List<CrearDetalleFacturaDto> detalles;
}
