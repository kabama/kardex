package com.kbm.kardex.domain.models.administrativo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

 

/**
 * Clase que mapea los errores basicos de las respuesta para login
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class ErrorLoginRequest
{
    
    private String error;
    private String message;
    private int status;
}

