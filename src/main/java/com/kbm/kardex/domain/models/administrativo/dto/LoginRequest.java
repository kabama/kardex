package com.kbm.kardex.domain.models.administrativo.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * clase que mapea los datos del login
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/25
 * @since 0.0.1 2020/10/25
 */
@Data
public class LoginRequest
{
    @Email(message = "Ingresa un email valido")
    @NotBlank(message = "El campo email es obligatorio")
    private String email;
    
    @NotBlank(message = "El campo password es obligatorio")
    @Min(message = "El password debe tener minimo 8 digitos", value = 8)
    @Max(message = "El password debe tener minimo 8 digitos", value = 8)
    private String password;
}
