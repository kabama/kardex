package com.kbm.kardex.domain.models.movimientos.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Clase para el manejo de la creacion de detalle de movimiento
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@Data
public class CrearDetalleMovimientoDto
{
    @NotNull(message = "El campo identificador factura no puede ser nulo")
    private Long idFactura;
    @NotNull(message = "El campo identificador detalle factura no puede ser nulo")
    private Long idDetalleFactura;
    @NotNull(message = "El campo identificador movimiento no puede ser nulo")
    private Long idMovimiento;
    @NotNull(message = "El campo tipo movimiento no puede ser nulo")
    private Long tipoMovimientoId;
    @NotNull(message = "El campo fecha no puede ser nulo")
    private LocalDate fechaMovimiento;
    @NotBlank(message = "El campo descripcion no puede ser nulo")
    private String  descripcion;
    @NotNull(message = "El campo valor unitario no puede ser nulo")
    private Double  valorUnitario;
    @NotNull(message = "El campo cantidad no puede ser nulo")    
    private Integer cantidad;
    @NotNull(message = "El campo valor no puede ser nulo")
    private Double  valor;
    
    @JsonIgnore
    private Integer cantidadSaldo;
    @JsonIgnore
    private Double  valorSaldo;
}
