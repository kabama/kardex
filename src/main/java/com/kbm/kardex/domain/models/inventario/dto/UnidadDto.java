package com.kbm.kardex.domain.models.inventario.dto;

import lombok.Data;

/**
 * Clase que mapea los tipos de unidades
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Data
public class UnidadDto
{
    private Long id;
    private String nombre;
    private String descripcion;
}