package com.kbm.kardex.domain.models.movimientos.dto.enumerador;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enumerador para identificar el tipo de movimiento
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@AllArgsConstructor
@Getter
public enum TipoMovimientoEnum
{
    INVENTARIO_INICIAL(1L), COMPRA(2L), VENTA(3L), DEVOLUCION_COMPRA(4L), DEVOLUCION_VENTA(5L);
    
    
    private Long tipo;
}
