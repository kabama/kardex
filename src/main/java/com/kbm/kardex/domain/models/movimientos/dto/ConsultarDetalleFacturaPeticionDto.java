package com.kbm.kardex.domain.models.movimientos.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Calse que mapea la peticion de consulta detalle factura
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Data
public class ConsultarDetalleFacturaPeticionDto
{
    @NotNull
    private Long idProducto;
    @NotNull
    private Long idMovimiento;
}
