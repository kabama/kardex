package com.kbm.kardex.domain.models.administrativo.dto;

import lombok.Data;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/26
 * @since 0.0.1 2020/10/26
 */
@Data
public class ProveedorDto
{
    private Long id;
    private String nombreCompleto;
    private String email;
}
