package com.kbm.kardex.domain.models.inventario.dto;

import lombok.Data;

/**
 * Clase que mapea los productos
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/22
 * @since 0.0.1 2020/10/22
 */
@Data
public class ProductoDto
{
    private Long   id;
    private String nombre;
    private String descripcion;
    
    private String  codigo;
    private Double  costo;
    private String  marca;
    private String  modelo;
    private Integer cantidad;
    private String  estado;
    private String  observacion;
}
