package com.kbm.kardex.domain.models.movimientos.dto;

import lombok.Data;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/28
 * @since 0.0.1 2020/10/28
 */
@Data
public class TipoMovimientoDto
{
    private long id;
    private String nombre;
}
