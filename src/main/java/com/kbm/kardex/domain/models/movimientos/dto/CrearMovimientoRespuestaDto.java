package com.kbm.kardex.domain.models.movimientos.dto;

import lombok.Data;

/**
 * DTO que mapea la respuesta de la creacion de un usuario
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@Data
public class CrearMovimientoRespuestaDto
{
    private Long id;
    private Integer minimo;
    private Integer maximo;
    
    private String productoReferencia;
    private String productoLocalizacion;
    private String productoNombre;
    private String productoUnidades;
    private String proveedorNombre;
}
