package com.kbm.kardex.domain.models.movimientos.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * DTO con los datos basicos para crear un movimiento
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/23
 * @since 0.0.1 2020/10/23
 */
@Data
public class CrearMovimientoPeticionDto
{
    
    @NotBlank(message = "El campo observacion no puede ser vacio ni nulo")
    private String  observaciones;
    @NotNull(message = "El campo minimo numero de productos no puede ser nulo")
    private Integer minimo;
    @NotNull(message = "El campo maximo numero de productos no puede ser nulo")
    private Integer maximo;
    @NotNull(message = "El campo identificador de producto no puede ser nulo")
    private Long    productoId;
    @NotNull(message = "El campo identificador de usuario no puede ser nulo")
    private Long    usuarioId;
    
}
