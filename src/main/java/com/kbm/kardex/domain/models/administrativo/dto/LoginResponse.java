package com.kbm.kardex.domain.models.administrativo.dto;

import java.util.List;

import lombok.Data;

@Data
public class LoginResponse
{
 private String token;
 private String email;
 List<String> roles;
}
