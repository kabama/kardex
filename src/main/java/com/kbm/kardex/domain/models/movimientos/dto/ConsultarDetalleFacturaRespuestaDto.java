package com.kbm.kardex.domain.models.movimientos.dto;

import java.time.LocalDate;

import lombok.Data;

/**
 * Clase que mapea los datos de las consultas de facturas para kardex
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/24
 * @since 0.0.1 2020/10/24
 */
@Data
public class ConsultarDetalleFacturaRespuestaDto
{
    private String numeroFactura;
    private Long idFactura;
    private Long idDetalleFactura;
    private Long idMovimiento;
    private Long tipoMovimientoId;
    private LocalDate fechaMovimiento;
    private String  descripcion;
    private Double  valorUnitario;
    private Integer cantidad;
    
    
    private String detalleKardex;
    private String fechaDetalleKardex;
}
