package com.kbm.kardex.domain.models.movimientos.dto;

import java.util.List;

import lombok.Data;

/**
 * @author Kaleth Bahena
 * @version 0.0.1 2020/10/27
 * @since 0.0.1 2020/10/27
 */
@Data
public class ConsultarMovimientoRespuestaDto
{
    private Long id;
    private Long idMovimiento;
    private Long idProducto;
    private Integer minimo;
    private Integer maximo;
    
    private String productoReferencia;
    private String productoLocalizacion;
    private String productoNombre;
    private String productoUnidades;
    private String proveedorNombre;
    List<DetalleMovimientoKardexDto> detalles;
}
