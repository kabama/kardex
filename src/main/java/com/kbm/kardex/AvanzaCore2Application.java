package com.kbm.kardex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.kbm.kardex.infrastucture.repository")
public class AvanzaCore2Application {

	public static void main(String[] args) {
		SpringApplication.run(AvanzaCore2Application.class, args);
	}

}
