package com.kbm.kardex.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatusModelo
{
    ACTIVO(1L), INACTIVO(0L);
    
    
    private Long status;
}
